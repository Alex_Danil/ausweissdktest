//: Playground - noun: a place where people can play

import UIKit

class Box<T> {
	func seal(_: T) {}
}

class PromiseResult: Thenable {
	case success(String,ManagerReceive)
	
	func onNext(_ result: ((String)->())) -> ManagerReceive? {
		switch self {
		case .success(let message, let manager):
			return manager
		}
		
	}
}




public protocol Thenable: class {
	/// The type of the wrapped value
	associatedtype T
	
	/// `pipe` is immediately executed when this `Thenable` is resolved
	func pipe(to: @escaping(Result<T>) -> Void)
	
	/// The resolved result or nil if pending.
	var result: Result<T>? { get }
}

public extension Thenable {
	/**
	The provided closure executes when this promise resolves.
	
	This allows chaining promises. The promise returned by the provided closure is resolved before the promise returned by this closure resolves.
	
	- Parameter on: The queue to which the provided closure dispatches.
	- Parameter body: The closure that executes when this promise fulfills. It must return a promise.
	- Returns: A new promise that resolves when the promise returned from the provided closure resolves. For example:
	
	firstly {
	URLSession.shared.dataTask(.promise, with: url1)
	}.then { response in
	transform(data: response.data)
	}.done { transformation in
	//…
	}
	*/
	func then<U: Thenable>( _ body: @escaping(T) throws -> U) -> Promise<U.T> {
		let rp = PromiseResult<U.T>(.pending)
		pipe {
			switch $0 {
			case .fulfilled(let value):
				
			case .rejected(let error):
				rp.box.seal(.rejected(error))
			}
		}
		return rp
	}





















class ManagerReceive {
	var message: String = "Message"
	
	static let manager = ManagerReceive()
	
	var result : Result?
	
	func sendMessage(_ string: String, stringReturned: ((String)->()) -> ManagerReceive) -> Result? {
		
		DispatchQueue(label: "message", qos: .background).asyncAfter(deadline: .now() + 3, execute: {
				self.result = Result.success(string, self)
		})
		
		if result != nil {
			switch result! {
			case .success(let message, _):
				print(message)
			}
			return result!
		} else {
			print("nil")
		}
		
		
	}
}

class AnotherManagerRecieveAndBack {

	func sendRecieveMessage() {
		ManagerReceive.manager.sendMessage("message1")?.onNext { (message) in
				print(message)
			}
//			?.sendMessage("message2")
//			.onNext { (message2) in
//				print(message2)
//			}?.sendMessage("message3")
//			.onNext { (message2) in
//				print(message2)
//			}?.sendMessage("message4")
//			.onNext { (message2) in
//				print(message2)
//		}
	}
}

AnotherManagerRecieveAndBack().sendRecieveMessage()
