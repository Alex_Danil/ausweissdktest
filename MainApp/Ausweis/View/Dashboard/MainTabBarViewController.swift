//
//  MainTabBarViewController.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 4/27/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
			super.viewDidLoad()
			setupTheme(.black)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MainTabBarViewController : ASWThemeProtocol {
	
	func setupTheme(_ theme: Theme) {
		switch theme {
		case .black:
			tabBar.barTintColor = .blackThree
			tabBar.unselectedItemTintColor = .titleBlack
			tabBar.tintColor = .turquoiseGreen2
		case .white:
			return
		}
	}
	
}
