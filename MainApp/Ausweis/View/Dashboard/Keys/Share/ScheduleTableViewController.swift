//
//  ScheduleTableViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import Ausweis_ioSDK

class ScheduleTableViewController: UITableViewController {
	
	@IBOutlet weak var startTimeField: UITextField!
	@IBOutlet weak var endTimeField: UITextField!
	
	let startDatePicker = UIDatePicker()
	let endDatePicker = UIDatePicker()
	
	var viewModel: ShareKeyViewModel!
	
	let disposeBag = DisposeBag()
	
	private var schedule : KeySchedule = KeySchedule()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let doneItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(endEditing))
		let doneItem2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(endEditing))
		
		startTimeField.inputAccessoryView = UIToolbar().ToolbarPiker(buttonItem: doneItem)
		startTimeField.inputView = startDatePicker
		
		endTimeField.inputAccessoryView = UIToolbar().ToolbarPiker(buttonItem: doneItem2)
		endTimeField.inputView = endDatePicker
		
		setupDatePickers()
		
		self.navigationItem.setHidesBackButton(true, animated: true)
		let addItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(saveSchedule))
		
		self.navigationItem.rightBarButtonItem = addItem
		
		let cancelItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel))
		
		self.navigationItem.leftBarButtonItem = cancelItem
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if indexPath.section == 1 {
			guard let index = schedule.days.index(of: indexPath.row)  else {
				schedule.days.append(indexPath.row)
				tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
				return
			}
			
			schedule.days.remove(at: index)
			tableView.cellForRow(at: indexPath)?.accessoryType = .none
		}
		
	}
	
	func setupDatePickers() {
		startDatePicker.datePickerMode = .time
		endDatePicker.datePickerMode = .time
		
		let formatter = DateFormatter()
		formatter.dateFormat = "HH:mm"
		
		startDatePicker.rx.value.asObservable().subscribe(onNext: { (date) in
			self.startTimeField.text = formatter.string(from: date)
			self.schedule.startTime = formatter.string(from: date)
		}).disposed(by: disposeBag)
		
		endDatePicker.rx.value.asObservable().subscribe(onNext: { (date) in
			self.endTimeField.text = formatter.string(from: date)
			self.schedule.endTime = formatter.string(from: date)
		}).disposed(by: disposeBag)
		
	}
	
}


extension ScheduleTableViewController {
	
	@objc func endEditing() {
		self.view.endEditing(true)
	}
	
	@objc func saveSchedule() {
		self.viewModel.input.schedule.onNext(self.schedule)
		self.navigationController?.popViewController(animated: true)
		
	}
	
	@objc func cancel() {
		self.dismiss(animated: true, completion: nil)
	}
}


