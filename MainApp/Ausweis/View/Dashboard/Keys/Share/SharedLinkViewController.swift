//
//  SharedLinkViewController.swift
//  Ausweis
//
//  Created by admin1 on 8/10/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SharedLinkViewController: UIViewController, NibViewModelProtocol {
	
	typealias ViewModel = ShareKeyViewModel

	var viewModel: ViewModel!
	
	required init(nibName nn: String, bundle: Bundle!, viewModel: ShareKeyViewModel) {
		super.init(nibName: nn, bundle: bundle)
		self.viewModel = viewModel
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	@IBOutlet weak var linkLabel: UILabel!
	
	@IBOutlet weak var copyLinkButton: UIButton!
	@IBOutlet weak var revokeLinkButton: UIButton!
	@IBOutlet weak var shareLinkButton: UIButton!
	
	
	private let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
			
			viewModel.output.keyShareLink
				.bind(to: linkLabel.rx.text)
				.disposed(by: disposeBag)

			
			copyLinkButton.rx.tap.asObservable()
				.withLatestFrom(viewModel.output.keyShareLink)
				.subscribe(onNext: { link in
					UIPasteboard.general.string = link
				})
			.disposed(by: disposeBag)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
