//
//  EmailShareViewController.swift
//  Ausweis
//
//  Created by admin1 on 8/8/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class EmailShareViewController: UIViewController, NibViewModelProtocol {
	var viewModel: ShareKeyViewModel!
	
	typealias ViewModel = ShareKeyViewModel
	
	@IBOutlet weak var emailTextField: UITextField!
	@IBOutlet weak var emailDescription: UILabel!
	@IBOutlet weak var shareButton: UIButton!
	
	let disposeBag = DisposeBag()
	
	required init(nibName nn: String, bundle: Bundle!, viewModel: ViewModel) {
		super.init(nibName: nn, bundle: bundle)
		self.viewModel = viewModel
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
			emailTextField.rx.text
				.subscribe(viewModel.input.emailString)
				.disposed(by: disposeBag)
		
		let saveItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: nil)
		
		saveItem.rx.tap
			.subscribe(viewModel.input.saveSchedule)
			.disposed(by: disposeBag)
		
		self.navigationItem.rightBarButtonItem = saveItem
		
		
		let loadingVC = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
		
		viewModel.isLoading.asObservable()
			.bind(onNext: { loading in
				if loading {
					self.add(loadingVC)
				} else {
					loadingVC.remove()
				}
			})
			.disposed(by: disposeBag)
		
		
		
		// Do any additional setup after loading the view.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
