//
//  SchedulesListViewController.swift
//  Ausweis
//
//  Created by admin1 on 8/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Ausweis_ioSDK

class SchedulesListViewController: UIViewController {

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var addScheduleButton: UIButton!
	@IBOutlet weak var headerView: UIView!
	
	var viewModel : ShareKeyViewModel! {
		didSet {
			observeViewModel()
		}
	}
	
	private var schedules: [KeySchedule]!

	let disposeBag = DisposeBag()
	override func viewDidLoad() {
		super.viewDidLoad()
		tableView.register(UINib(nibName: "ScheduleTableViewCell", bundle: nil) , forCellReuseIdentifier: "ScheduleTableViewCell")
		tableView.tableHeaderView = headerView
		tableView.tableFooterView = UIView()
		
		addScheduleButton.rx.tap
			.bind(onNext: { _ in
				DashboardRouter().route(to: .schedule, from: self.parent, parameters: self.viewModel)
			})
			.disposed(by: disposeBag)
		
		// Do any additional setup after loading the view.
	}
	
	func observeViewModel(){
		viewModel.output.schedules
			.bind(onNext: { (schedules) in
				self.schedules = schedules
				self.tableView.reloadData()
				//				self.tableView.reloadSections([2], with: .none)
			})
			.disposed(by: disposeBag)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
}

extension SchedulesListViewController : UITableViewDelegate {
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return schedules?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 60
	}
}


extension SchedulesListViewController : UITableViewDataSource {
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleTableViewCell", for: indexPath) as! ScheduleTableViewCell
		
		cell.setSchedule(schedules[indexPath.row])
		
		return cell
	}
}
