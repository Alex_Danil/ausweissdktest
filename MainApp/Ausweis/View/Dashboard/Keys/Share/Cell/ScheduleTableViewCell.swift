//
//  ScheduleTableViewCell.swift
//  Ausweis
//
//  Created by admin1 on 7/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import Ausweis_ioSDK

class ScheduleTableViewCell: UITableViewCell {
	
	@IBOutlet weak var timeLabel: UILabel!
	@IBOutlet weak var daysLabel: UILabel!
	
	let weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setSchedule(_ schedule: KeySchedule) {
		timeLabel.text = schedule.startTime + " - " + schedule.endTime
		daysLabel.text = schedule.days.map { weekdays[$0] }.reduce("") { $0 + ", " + $1 }

	}

}
