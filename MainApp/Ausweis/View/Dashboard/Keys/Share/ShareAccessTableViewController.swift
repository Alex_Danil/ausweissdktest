//
//  ShareAccessTableViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import Ausweis_ioSDK

class ShareAccessTableViewController: UITableViewController {
	
	@IBOutlet weak var activityAccessSwitch: UISwitch!
	@IBOutlet weak var oneTimeKeySwitch: UISwitch!
	@IBOutlet weak var periodSwitch: UISwitch!
	@IBOutlet weak var startDateTextField: UITextField!
	@IBOutlet weak var endDateTextField: UITextField!
	@IBOutlet weak var addScheduleButton: UIButton!
	
	let startDatePicker = UIDatePicker()
	let endDatePicker = UIDatePicker()
	
	var viewModel : ShareKeyViewModel!
	let disposeBag = DisposeBag()
	

	
	override func viewDidLoad() {
		super.viewDidLoad()

		guard let scheduleListController = childViewControllers.first as? SchedulesListViewController else  {
			fatalError("Check storyboard for missing LocationTableViewController")
		}

		scheduleListController.viewModel = viewModel
		
		setupDatePickers()
		
		viewModel.output.schedules
			.bind(onNext: { (schedules) in
				if let _frame = self.tableView.tableFooterView?.frame {
					let frame = CGRect(x: _frame.minX, y: _frame.minY, width: _frame.width, height:  CGFloat(schedules.count * 60) + 44.0)
					self.tableView.tableFooterView?.frame = frame
					self.tableView.tableFooterView = self.tableView.tableFooterView
				}
				
			})
			.disposed(by: disposeBag)
		
		periodSwitch.rx.isOn.asObservable()
			.subscribe(onNext: { [weak self] value in
				self?.viewModel.input.isLimited.onNext(value)
				self?.tableView.reloadData()
			})
			.disposed(by: disposeBag)
		
		activityAccessSwitch.rx.isOn
			.bind(to: viewModel.input.isActivityAccess)
			.disposed(by: disposeBag)
		
		oneTimeKeySwitch.rx.isOn
			.bind(to: viewModel.input.isOneTimeKey)
			.disposed(by: disposeBag)
		
		let loadingVC = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
		
		viewModel.isLoading.observeOn(MainScheduler.asyncInstance)
			.bind(onNext: { loading in
				if loading {
					self.add(loadingVC)
				} else {
					loadingVC.remove()
				}
			})
			.disposed(by: disposeBag)
		
		viewModel.output.keyShareLink.bind { [weak self] _ in
			DashboardRouter().route(to: .shareLink, from: self, parameters: self?.viewModel)
			}.disposed(by: disposeBag)
		
	}
	
	func setupDatePickers() {
		
		let doneItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(endEditing))
		let doneItem2 = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(endEditing))
		
		let saveItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: nil)
		
		saveItem.rx.tap.withLatestFrom(viewModel.input.shareType)
			.bind(onNext: { [weak self] shareType in
				if shareType == .email {
					DashboardRouter().route(to: .shareEmail , from: self, parameters: self?.viewModel)
				} else {
					self?.viewModel.input.saveSchedule.onNext(())
				}
			})
			.disposed(by: disposeBag)
		
		self.navigationItem.rightBarButtonItem = saveItem
		
		startDateTextField.inputAccessoryView = UIToolbar().ToolbarPiker(buttonItem: doneItem)
		startDateTextField.inputView = startDatePicker
		
		endDateTextField.inputAccessoryView = UIToolbar().ToolbarPiker(buttonItem: doneItem2)
		endDateTextField.inputView = endDatePicker
		
		startDatePicker.datePickerMode = .dateAndTime
		endDatePicker.datePickerMode = .dateAndTime
		
		viewModel.output.limitStartString
			.bind(to: startDateTextField.rx.text)
			.disposed(by: disposeBag)
		
		viewModel.output.limitEndString
			.bind(to: endDateTextField.rx.text)
			.disposed(by: disposeBag)
		
		startDatePicker.rx.date
			.bind(to: viewModel.input.limitStart)
			.disposed(by: disposeBag)
		
		endDatePicker.rx.date
			.bind(to: viewModel.input.limitEnd)
			.disposed(by: disposeBag)
	}
	
	
	
	// MARK: - Table view data source
	
	
	
	override func numberOfSections(in tableView: UITableView) -> Int {
		return 3
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		
		switch section {
		case 0:
			return 2
		case 1:
			return periodSwitch.isOn ? 3 : 1
		
		default:
			return 0
		}
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.section == 1 && (indexPath.row == 1 || indexPath.row == 2) {
			
		}
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.section {
		case 2:
			let _cell : UITableViewCell
			let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleTableViewCell", for: indexPath)
//			dequeueReusableCell(withIdentifier: "ScheduleTableViewCell") as! ScheduleTableViewCell
//			cell.setSchedule(schedules[indexPath.row])
			_cell = cell
			return _cell
		default:
			return super.tableView(tableView, cellForRowAt: indexPath)
			
		}
	}
	
	
	@objc func endEditing() {
		self.view.endEditing(true)
	}
	
}
