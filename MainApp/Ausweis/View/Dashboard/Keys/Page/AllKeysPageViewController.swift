//
//  AllKeysPageViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/27/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

class AllKeysPageViewController: UIPageViewController {
	
	private var ownerKeys: KeysViewController!
	private var sharedKeys: KeysViewController!
	
	private var viewModel: AllKeysViewModel!
	
	let searchController = UISearchController(searchResultsController: nil)
	
	var keysTableScrolled : ((_ scrollView: UIScrollView) -> ())!

	override func viewDidLoad() {
		super.viewDidLoad()

	
		let service = KeysService()

		viewModel = AllKeysViewModel(keysService: service)
		
		ownerKeys = KeysViewController(nibName: "KeysViewController", bundle: nil, viewModel: viewModel, source: .owner )
		sharedKeys = KeysViewController(nibName: "KeysViewController", bundle: nil, viewModel: viewModel, source: .shared)
		
		ownerKeys.keysTableScrolled = keysTableScrolled
		sharedKeys.keysTableScrolled = keysTableScrolled
		
		setViewControllers([ownerKeys], direction: .forward, animated: true, completion: nil)
		
		viewModel.input.loadAllKeys.onNext(())
		
		// Do any additional setup after loading the view.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	public func moveController(fromItem item: HeaderItem) {

		switch item {
		case .owner:
			setViewControllers([ownerKeys], direction: .reverse, animated: true, completion: nil)
		case .shared:
			setViewControllers([sharedKeys], direction: .forward, animated: true, completion: nil)
		}
	}
	
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}

extension AllKeysPageViewController: ASWThemeProtocol {
	
	func setupTheme(_ theme: Theme) {
		self.view.backgroundColor = theme.loginBackgroundColor
		self.navigationController?.navigationBar.barStyle = theme.barStyle
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
	
}
