//
//  AllKeysViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/2/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

class AllKeysViewController: UIViewController {
	
	@IBOutlet weak var containerView: UIView!
	@IBOutlet weak var headerView: KeysHeaderView!
	@IBOutlet weak var titleView: UILabel!
	@IBOutlet weak var titleViewConstraint: NSLayoutConstraint!
	
	var navigationTitleLabel: UILabel!
	
	var allKeysViewController : AllKeysPageViewController!
	
	@IBOutlet weak var fakeTableView: UIScrollView!
	
	let searchController = UISearchController(searchResultsController: nil)
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		allKeysViewController.keysTableScrolled = scrollViewDidScroll(_:)
		
		headerView.setupDefaultItem(.shared) { [unowned self] (item) in
			self.allKeysViewController.moveController(fromItem: item)
		}
		
		setupNavigationTitle()
		setupTheme(.black)
		// Do any additional setup after loading the view.
	}
	
	func setupNavigationTitle() {
		navigationTitleLabel = UILabel()
		navigationTitleLabel.text = "Ausweis"
		navigationTitleLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
		navigationTitleLabel.alpha = 0
		navigationItem.titleView = navigationTitleLabel
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let keysPageViewController = segue.destination as? AllKeysPageViewController {
			allKeysViewController = keysPageViewController
			allKeysViewController.keysTableScrolled = scrollViewDidScroll(_:)
		}
	}
	
	var canTransitionToLarge = false
	var canTransitionToSmall = true
	
	func scrollViewDidScroll(_ scrollView: UIScrollView){
		
		if canTransitionToLarge && scrollView.contentOffset.y < 0 {
			self.titleViewConstraint.constant = -self.titleView.frame.height
			
			UIView.animate(withDuration: 0.3) {
				self.navigationTitleLabel.alpha = 0
				self.view.layoutIfNeeded()
			}
			canTransitionToLarge = false
			canTransitionToSmall = true
		} else if canTransitionToSmall && scrollView.contentOffset.y > 52 {  // 44 - searchBarheight
			self.titleViewConstraint.constant = 0
			
			UIView.animate(withDuration: 0.3) {
				self.navigationTitleLabel.alpha = 1
				self.view.layoutIfNeeded()
			}
			canTransitionToLarge = true
			canTransitionToSmall = false
		}
	}
	
	
}


extension AllKeysViewController: ASWThemeProtocol {
	
	func setupTheme(_ theme: Theme) {
		containerView.backgroundColor = theme.loginBackgroundColor
		navigationTitleLabel.textColor = .white
		navigationController?.navigationBar.barStyle = theme.barStyle
		navigationController?.navigationBar.isTranslucent = false
		navigationController?.navigationBar.backgroundColor = .black
		navigationController?.navigationBar.tintColor = .white
		navigationController?.navigationBar.shadowImage = UIImage()
		
	}
	
}
