//
//  KeysHeaderView.swift
//  Ausweis
//
//  Created by admin1 on 7/2/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

enum HeaderItem: Int {
	case owner 	= 0
	case shared = 1
}

enum SliderDirection {
	case left
	case right
}

class KeysHeaderView: UIView {

	@IBOutlet var contentView: UIView!
	@IBOutlet weak var backgroundView: UIView!
	@IBOutlet weak var separatorView: UIView!
	
	@IBOutlet weak var myLocksButton: UIButton!
	@IBOutlet weak var sharedLocksButton: UIButton!
	
	@IBOutlet weak var sliderView: UIView!
	
	private var changeAction: ((_ headerItem: HeaderItem)->())!
	
	@IBOutlet weak var leftConstraint: NSLayoutConstraint!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		initialize()
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		initialize()
	}
	
	private func initialize() {
		Bundle.main.loadNibNamed("KeysHeaderView", owner: self, options: nil)
		addSubview(contentView)
		contentView.frame = self.bounds
		
	}
	
	func setupDefaultItem(_ headerItem: HeaderItem, changeAction action: @escaping (_ headerItem: HeaderItem)->() ) {
		
		changeAction = action
		handleSelection(onItem: headerItem)
	}
	
	@IBAction func headerButtonTapped(_ sender: UIButton) {
		sender.setTitleColor(sender.titleColor(for: .normal)?.withAlphaComponent(0.7), for: .normal)
		
		if let item = HeaderItem(rawValue: sender.tag) {
			handleSelection(onItem: item)
		}
	}
	
	private func handleSelection(onItem headerItem: HeaderItem) {
		changeAction(headerItem)
		
		switch headerItem {
			
		case .owner:
			moveSlider(toDirection: .left)
			sharedLocksButton.setTitleColor(sharedLocksButton.titleColor(for: .normal)?.withAlphaComponent(0.4), for: .normal)
		case .shared:
			moveSlider(toDirection: .right)
			myLocksButton.setTitleColor(myLocksButton.titleColor(for: .normal)?.withAlphaComponent(0.4), for: .normal)
		}
	}
	
	private func moveSlider(toDirection  moveDirection: SliderDirection) {
		switch moveDirection {
		case .left:
			leftConstraint.priority = UILayoutPriority(rawValue: 999)
		case .right:
			leftConstraint.priority = UILayoutPriority(rawValue: 997)
		}
		
		UIView
			.animate(withDuration: 0.3) {
			self.layoutIfNeeded()
		}
		
	}
	
	
}
