//
//  TableViewCell.swift
//  Ausweis
//
//  Created by admin1 on 7/3/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift

class KeyTableViewCell: UITableViewCell {
	
	@IBOutlet weak var nameLabel: UILabel!
	
	@IBOutlet weak var alarmButton: UIButton!
	
	@IBOutlet weak var sliderView: MMSlidingButton!
	
	@IBOutlet weak var loadingView: UIView!
	@IBOutlet weak var rightLoadingConstraint: NSLayoutConstraint!
	@IBOutlet weak var leftLoadingConstraint: NSLayoutConstraint!
	
	private var viewModel: KeyCellViewModel!
	
	let disposeBag = DisposeBag()
	
	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
		
		sliderView.delegate = self
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
	func setupViewModel(_ vm: KeyCellViewModel) {
		viewModel = vm
		viewModel.output.name.asObservable().bind(to: nameLabel.rx.text).disposed(by: disposeBag)
		viewModel.output.alarm.asObservable().bind(to: alarmButton.rx.isEnabled).disposed(by: disposeBag)
		
		
		viewModel.output.isOpenClose.asObservable()
			.bind { [weak sliderView] isOpenClose in
				sliderView?.slideType =  isOpenClose ? .openClose : .open
			}
			.disposed(by: disposeBag)
//		alarm.asObservable().bind(to: alarmButton.rx.isEnabled).disposed(by: disposeBag)
		
		viewModel.output.openResponse
			.observeOn(MainScheduler.asyncInstance)
			.subscribe(onNext: { (success) in
				if success {
					self.stopAnimation()
				}
			})
			.disposed(by: disposeBag)
		
		viewModel.output.errorsObservable
			.observeOn(MainScheduler.asyncInstance)
			.subscribe(onNext: { (error) in
				self.stopAnimation()
			})
			.disposed(by: disposeBag)
	}
	
	
	private var animationCounter: Int?
	func animate() {
		guard let counter = animationCounter else {
			return
		}
		if counter % 2 == 0 {
			leftLoadingConstraint.priority = UILayoutPriority(rawValue: 999)
			rightLoadingConstraint.priority = UILayoutPriority(rawValue:  998)
		} else {
			leftLoadingConstraint.priority =  UILayoutPriority(rawValue: 998)
			rightLoadingConstraint.priority =  UILayoutPriority(rawValue: 999)
		}
		
		UIView.animate(withDuration: 1.2, delay: 0, options: [.curveEaseInOut], animations: {
			self.layoutIfNeeded()
		}) { (_) in
			if self.animationCounter != nil {
				self.animationCounter = self.animationCounter!  + 1
				self.animate()
			}
		}
		
	}
	
	func stopAnimation() {
		animationCounter = nil
		sliderView.reset()
		loadingView.alpha = 0
		isUserInteractionEnabled = true
	}
	
}


extension KeyTableViewCell: SlideButtonDelegate {
	func buttonStatus(status: SlideStatus, action: SlideAction!, sender: MMSlidingButton) {
		switch status {
		case .opened, .closed:
			loadingView.alpha = 1
			animationCounter = 0
			animate()
			if action == .open {
				viewModel.input.openKey.onNext(.open)
			} else if action == .close {
				viewModel.input.openKey.onNext(.close)
			}
		case .cancelled:
			animationCounter = nil
			break
		
		}
	}
	
	
	func buttonStatus(status: SlideStatus, sender: MMSlidingButton) {
		
	}
	
}
