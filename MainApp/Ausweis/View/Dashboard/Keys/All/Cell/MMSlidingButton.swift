//
//  MMSlidingButton.swift
//  MMSlidingButton
//
//  Created by Mohamed Maail on 6/7/16.
//  Copyright © 2016 Mohamed Maail. All rights reserved.
//

import Foundation
import UIKit

enum SlideStatus {
	case opened
	case closed
	case cancelled
}

enum SlideType {
	case open
	case openClose
}

enum SlideAction {
	case open
	case close
	case `in`
	case out
}


protocol SlideButtonDelegate{
	func buttonStatus(status:SlideStatus, action: SlideAction!, sender:MMSlidingButton)
}

@IBDesignable class MMSlidingButton: UIView{
	
	var delegate: SlideButtonDelegate?
	
	@IBInspectable var dragPointWidth: CGFloat = 70 {
		didSet{
			setStyle()
		}
	}
	
	@IBInspectable var dragPointColor: UIColor = UIColor.darkGray {
		didSet{
			setStyle()
		}
	}
	
	@IBInspectable var buttonColor: UIColor = UIColor.gray {
		didSet{
			setStyle()
		}
	}
	
	@IBInspectable var imageName: UIImage = UIImage() {
		didSet{
			setStyle()
		}
	}
	
	
	@IBInspectable var dragPointTextColor: UIColor = UIColor.white {
		didSet{
			setStyle()
		}
	}
	
	
	@IBInspectable var buttonCornerRadius: CGFloat = 30 {
		didSet{
			setStyle()
		}
	}
	
	@IBOutlet weak var openCloseView: UIView!
	
	@IBOutlet weak var slideTitleView: UIView!

	
	@IBOutlet weak var openImageView: UIImageView! {
		didSet {
			openImageView.image? = (openImageView.image?.withRenderingMode(.alwaysTemplate))!
			openImageView.tintColor = openImageView.tintColor
		}
	}
	@IBOutlet weak var closeImageView: UIImageView!{
		didSet {
			closeImageView.image? = (closeImageView.image?.withRenderingMode(.alwaysTemplate))!
			closeImageView.tintColor = closeImageView.tintColor
		}
	}

	
	var dragPoint = UIImageView()
	var imageView = UIImageView()
	var unlocked = false
	var layoutSet = false
	var slideType : SlideType = .openClose
	
	var panGestureRecognizer: UIPanGestureRecognizer!
	
	override init (frame : CGRect) {
		super.init(frame : frame)
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
	}
	
	override func layoutSubviews() {
		if !layoutSet{
			self.setUpButton()
			self.layoutSet = true
		}
	}
	
	func setStyle(){
		
		dragPoint.frame.size.width = dragPointWidth
		dragPoint.backgroundColor = dragPointColor
		backgroundColor = self.buttonColor
		imageView.image = imageName
		imageView.clipsToBounds = false
		dragPoint.layer.cornerRadius = buttonCornerRadius
		layer.cornerRadius = buttonCornerRadius
	}
	
	func setUpButton(){
		backgroundColor = dragPointColor
		
		dragPoint = UIImageView(frame: CGRect(x: 0, y: 0, width: self.dragPointWidth, height: self.frame.size.height))
		dragPoint.contentMode = .center
		dragPoint.image = self.imageName
		dragPoint.layer.cornerRadius = buttonCornerRadius
		dragPoint.isUserInteractionEnabled = true
		addSubview(dragPoint)
		
		bringSubview(toFront: dragPoint)
		layer.masksToBounds = false
		
		// start detecting pan gesture
		panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.panDetected(sender:)))
		panGestureRecognizer.minimumNumberOfTouches = 1
		panGestureRecognizer.maximumNumberOfTouches = 1
		
		self.dragPoint.addGestureRecognizer(panGestureRecognizer)
	}

	
	
	@objc
	func panDetected(sender: UIPanGestureRecognizer){
		var translatedPoint = sender.location(in: self)
		
		switch slideType {
		case .openClose:
			if translatedPoint.x + self.dragPointWidth / 2 > self.frame.size.width {
				translatedPoint = CGPoint(x: self.frame.size.width - self.dragPointWidth, y: translatedPoint.y)
				sender.view?.frame.origin.y = translatedPoint.y - (self.dragPointWidth / 2)
				sender.view?.frame.origin.x = self.frame.size.width - self.dragPointWidth
				
				if sender.view!.frame.origin.y <= openCloseView.frame.origin.y {
					finishDragOpenClose(sender: sender, translatedPoint: translatedPoint, slideStatus: .opened)
				} else if sender.view!.frame.maxY >= openCloseView.frame.maxY {
					finishDragOpenClose(sender: sender, translatedPoint: translatedPoint, slideStatus: .closed)
				} else {
					finishDragOpenClose(sender: sender, translatedPoint: translatedPoint, slideStatus: .cancelled)
				}
				
			} else {
				openCloseView.alpha = (translatedPoint.x - (self.dragPointWidth / 2)) / (self.frame.width - (self.dragPointWidth * 2) )
				fallthrough
			}
			
		case .open :
			translatedPoint  = CGPoint(x: translatedPoint.x, y: self.center.y)
			self.slideTitleView.alpha = 1.0 - (translatedPoint.x - (self.dragPointWidth / 2)) / (self.frame.width - (self.dragPointWidth * 2) )
			if sender.state == .changed  {
				if (translatedPoint.x - self.dragPointWidth <= 0) ||
					(translatedPoint.x + self.dragPointWidth / 2 < (self.frame.size.width) ) {
					sender.view?.frame.origin.x = translatedPoint.x - sender.view!.frame.width / 2
				}
			} else if sender.state == .ended {
				finishDragOpen(sender: sender, translatedPoint: translatedPoint)
			}
		}
	}
	
	func finishDragOpenClose(sender: UIPanGestureRecognizer, translatedPoint: CGPoint, slideStatus: SlideStatus) {
		
		switch slideStatus {
		case .closed :
			if sender.state == .ended {
					delegate?.buttonStatus(status: slideStatus, action: .close, sender: self)
			}
			sender.view?.frame.origin.y = self.openCloseView.frame.maxY - sender.view!.frame.height
		case .opened :
			if sender.state == .ended {
					delegate?.buttonStatus(status: slideStatus, action: .open, sender: self)
			}
			sender.view?.frame.origin.y = self.openCloseView.frame.origin.y
		case .cancelled:
			break
		}
		
	}
	
	
	func finishDragOpen(sender: UIPanGestureRecognizer, translatedPoint: CGPoint) {
		let velocityX = sender.velocity(in: self).x * 0.2
		let animationDuration:Double = abs(Double(velocityX) * 0.0002) + 0.1
		

		if translatedPoint.x + self.dragPointWidth * 1.5 > (self.frame.size.width) && slideType != .openClose {
			UIView.animate(withDuration:animationDuration, animations: {
				self.dragPoint.frame = CGRect(x: self.frame.size.width - (self.dragPoint.frame.size.width),
																			y:  self.dragPoint.frame.origin.y,
																			width: self.dragPoint.frame.width,
																			height: self.dragPoint.frame.height)
			})
			
			panGestureRecognizer.isEnabled = false
			delegate?.buttonStatus(status: .opened, action: .open, sender: self)
			//				unlock()
		} else {
			
			delegate?.buttonStatus(status: .cancelled, action: nil, sender: self)
			self.reset()
		}
	}
	
}

extension MMSlidingButton {
	
	
	func animationFinished(){
		if !unlocked{
			reset()
		}
	}
	
	//lock button animation (SUCCESS)
	func unlock(){
		UIView.animate(withDuration: 0.4, delay: 0.7, options: .curveEaseIn, animations: {
			self.dragPoint.frame = CGRect(x: 0, y: 0, width: self.dragPoint.frame.size.width, height: self.dragPoint.frame.size.height)
		}) { (Status) in
			self.dragPoint.isUserInteractionEnabled = true
		}
	}
	
	//reset button animation (RESET)
	func reset(){
		panGestureRecognizer.isEnabled = true
		
		switch slideType {
		case .open:
			UIView.transition(with: self, duration: 0.4, options: .curveEaseOut, animations: {
				self.dragPoint.frame = CGRect(x: 0, y: 0, width: self.dragPoint.frame.size.width, height: self.dragPoint.frame.size.height)
				self.openCloseView.alpha = 0
				self.slideTitleView.alpha = 1
				
			}) { (Status) in
				self.unlocked = false
			}
		case .openClose:
			resetOpenClose(status: .opened)
		}
		
	}
	
	func resetOpenClose(status: SlideStatus){
		UIView.transition(with: self, duration: 0.4, options: .curveEaseOut, animations: {
			self.dragPoint.center = self.openCloseView.center
		}) { _ in
			if status != .cancelled {
				UIView.transition(with: self, duration: 0.4, options: .curveEaseOut, animations: {
					self.openCloseView.alpha = 0
					self.slideTitleView.alpha = 1
					self.dragPoint.frame = CGRect(x: 0, y: 0, width: self.dragPoint.frame.size.width, height: self.dragPoint.frame.size.height)
				})
			}
		}
	}
	
	
}
