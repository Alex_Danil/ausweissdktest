//
//  KeysViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/27/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Ausweis_ioSDK

enum KeysSource: String {
	case owner = "Owner"
	case shared = "Shared"
}

class KeysViewController: UIViewController, NibViewModelProtocol {
	typealias ViewModel = AllKeysViewModel
	var viewModel: ViewModel!
	
	@IBOutlet weak var keysTableView: UITableView!
	@IBOutlet weak var searchBar: UISearchBar!
	
	private var source: KeysSource!
//	private var keys: [Key]!
	
	var keysTableScrolled : ((_ scrollView: UIScrollView) -> ())!
	let disposeBag: DisposeBag = DisposeBag()
	
	convenience init(nibName nn: String, bundle nibBundleOrNil: Bundle? = nil, viewModel: ViewModel, source: KeysSource ) {
		self.init(nibName: nn, bundle: nibBundleOrNil, viewModel: viewModel)
		self.source = source
	}

	required init(nibName nn:String, bundle: Bundle!, viewModel: ViewModel) {
		super.init(nibName: nn, bundle: bundle)
		self.viewModel = viewModel
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupTableView()
		observeKeys(forSource: source)
		keysTableView.contentOffset = CGPoint(x:0, y:48)
		
		searchBar.rx.value
			.asObservable()
			.ignoreNil()
			.subscribe (viewModel.input.searchQuery)
			.disposed(by: disposeBag)
		
		viewModel.output.allKeysResult
			.delay(0.7, scheduler: MainScheduler.asyncInstance)
			.subscribe(onNext: { (keys) in
					if self.keysTableView.refreshControl?.isRefreshing ?? false {
						self.keysTableView.refreshControl?.endRefreshing()
					}
			})
			.disposed(by: disposeBag)
		
		viewModel.output.errorsObservable
			.observeOn(MainScheduler.asyncInstance)
			.subscribe(onNext: { (error) in
				if self.keysTableView.refreshControl?.isRefreshing ?? false {
					self.keysTableView.refreshControl?.endRefreshing()
				}
				
				DashboardRouter.showAlert(message: error.errors.first?.message, fromVC: self)
			})
			.disposed(by: disposeBag)
	}
	
	func observeKeys(forSource src: KeysSource) {
		var responseObservable : Observable<[Key]>!
		
		switch src {
		case .owner:
			responseObservable = viewModel.output.ownerKeysResult
		case .shared:
            responseObservable = viewModel.output.sharedKeysResult
        }
		
		bindTableView(keysTableView, observable: responseObservable)
		bindCellSelected(keysTableView)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}
	
	
	func setupTableView() {
		keysTableView.delegate = nil
		keysTableView.dataSource = nil
		keysTableView.rx.setDelegate(self).disposed(by: disposeBag)
		keysTableView.register(KeyTableViewCell.nib(),
													 forCellReuseIdentifier: KeyTableViewCell.identifier)
		
		keysTableView.refreshControl = UIRefreshControl()
		keysTableView.rx.didScroll.observeOn(MainScheduler.asyncInstance)
			.subscribe { _ in
					self.view.endEditing(false)
					self.keysTableScrolled(self.keysTableView)
			}
			.disposed(by: disposeBag)
		
		keysTableView.refreshControl!.addTarget(self, action: #selector(reloadKeys), for: .valueChanged)
		

//        keysTableView.refreshControl?.rx.

	}
	
	@objc func reloadKeys(){
		viewModel.input.loadAllKeys.onNext(())
	}
	
	private func bindTableView(_ tableView: UITableView, observable: Observable<[Key]>) {

		observable.bind(to: tableView.rx.items(cellIdentifier: KeyTableViewCell.identifier, cellType: KeyTableViewCell.self)){ index,model,cell in
				let cellViewModel = KeyCellViewModel(key: model)
				cell.setupViewModel(cellViewModel)
			
		}.disposed(by: disposeBag)
		
	}
	
	
	private func bindCellSelected(_ tableView: UITableView) {
		tableView.rx.modelSelected(Key.self)
			.observeOn(MainScheduler.asyncInstance)
			.subscribe(onNext: { (model) in
					DashboardRouter().route(to: .detail, from: self, parameters: model.lockInfo)
			})
			.disposed(by: disposeBag)
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
}

extension KeysViewController : UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 88 + 82
	}
	
//	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//		DashboardRouter().route(to: .detail, from: self, parameters: nil)
//	}
	
}

