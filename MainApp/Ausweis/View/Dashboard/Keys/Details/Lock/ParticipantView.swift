//
//  ParticipantView.swift
//  Ausweis
//
//  Created by admin1 on 7/6/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import Ausweis_ioSDK

class ParticipantView: UIView {

	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var avatarView: UIImageView!

	
	func setupParticipant(_ participant: Participant) {
		isHidden = false
		nameLabel.text = participant.nameDisplayed
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "E, d MMM yyyy HH:mm"
		
		guard let date = participant.lastUsed else {
			dateLabel.text = "Never used"
			return
		}
		
		dateLabel.text = dateFormatter.string(from:date)
	}
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
