//
//  LockDetailsViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/6/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import Ausweis_ioSDK
import RxSwift
import RxCocoa
import GoogleMaps

class LockDetailsViewController: UIViewController {
	@IBOutlet weak var limitationView: UIView!
	@IBOutlet weak var mapView: GMSMapView!
	
	@IBOutlet weak var goButton: UIButton! {
		didSet{
			goButton.semanticContentAttribute = UIApplication.shared
				.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
		}
	}
	
	@IBOutlet weak var shareStackView: UIStackView!

	@IBOutlet weak var scrollView: UIScrollView!
	@IBOutlet weak var stackView: UIStackView!
	
	@IBOutlet weak var addressLabel: UILabel!
	@IBOutlet weak var notificationsSwitch: UISwitch!
	@IBOutlet weak var goToActivityButton: UIButton!
	
	var viewModel : LockViewModel!
	
	@IBOutlet weak var participantsStackView: UIStackView!
	@IBOutlet weak var showParticipantsButton: UIButton!
	@IBOutlet weak var participantsCountLabel: UILabel!
	@IBOutlet var participantViews: [ParticipantView]!
	
	@IBOutlet weak var settingsStackView: UIStackView!
	let disposeBag = DisposeBag()
	
	required init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, lock:LockResponse) {
		let service = LocksService()
		viewModel = LockViewModel(keysService: service, lock: lock)
		
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		bindLock()
		bindParticipants()
		
		viewModel.output.accessType
			.subscribe(onNext: setAccessType(_:))
			.disposed(by: disposeBag)
		
		stackView.rx.observe(CGRect.self, #keyPath(UIView.frame)).ignoreNil()
			.subscribe(onNext: { (rect) in
				self.scrollView.contentSize = rect.size
				print(rect.size)
			})
			.disposed(by: disposeBag)
		
		
		viewModel.input.loadLockDetails.onNext(())
		
		setupTheme(.black)
		
		goToActivityButton.rx.tap
			.bind() { _ in
				DashboardRouter().route(to: .history, from: self, parameters: self.viewModel)
			}
			.disposed(by: disposeBag)
		// Do any additional setup after loading the view.
		
	}
	

	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		self.scrollView.contentSize = stackView.frame.size

	}
	
	@IBAction func shareKey(_ sender: Any) {
		let controller = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
		
		let shareEmailAction = UIAlertAction(title: "Share key to user", style: .default, handler: handleShareEmailAction(_ :))
		
		let shareLinkAction = UIAlertAction(title: "Share key via link", style: .default, handler: handleShareLinkAction(_ :))
		
		let shareWebLinkAction = UIAlertAction(title: "Share key via web app", style: .default, handler: handleShareWebLinkAction(_ :))
		
		let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
		
		controller.addAction(shareEmailAction)
		controller.addAction(shareLinkAction)
		controller.addAction(shareWebLinkAction)
		controller.addAction(cancelAction)
		
		controller.view.tintColor = UIColor.turquoiseGreen2
		controller.view.backgroundColor = UIColor.blackTwo
		
		present(controller, animated: true, completion: nil)
		
	}
	
	func handleShareEmailAction(_ action: UIAlertAction) {
		DashboardRouter().route(to: .shareAccess, from: self, parameters: ["shareType": ShareKeyViewModel.ShareType.email, "lockID" : try! viewModel.output.lockID.value()])
	}
	
	func handleShareLinkAction(_ action: UIAlertAction) {
		DashboardRouter().route(to: .shareAccess, from: self, parameters: ["shareType": ShareKeyViewModel.ShareType.link, "lockID" : try! viewModel.output.lockID.value()])
	}
	
	func handleShareWebLinkAction(_ action: UIAlertAction) {
		DashboardRouter().route(to: .shareAccess, from: self, parameters: ["shareType": ShareKeyViewModel.ShareType.webApp, "lockID" : viewModel.output.lockID])
	}
	
}

//MARK: - Bindings

extension LockDetailsViewController {
	private func bindLock() {
		viewModel.output.address
			.ignoreNil()
			.bind(to: addressLabel.rx.text)
			.disposed(by: disposeBag)
		
		viewModel.output.isNotificationsOn
			.bind(to: notificationsSwitch.rx.isOn)
			.disposed(by: disposeBag)
		
		viewModel.output.lockName
			.bind(to: navigationItem.rx.title)
			.disposed(by: disposeBag)
		
		viewModel.output.location
			.bind(onNext: setupMapView(_:))
			.disposed(by: disposeBag)
	}
	
	private func bindParticipants() {
		viewModel.output.lockParticipants
			.subscribe(onNext: setupParticipants(_:))
			.disposed(by: disposeBag)
	}
	
	func setAccessType(_ accessType: LockAccessType) {
		DispatchQueue.main.async {
			switch accessType {
			case .guest:
				self.shareStackView.isHidden = true
				self.settingsStackView.isHidden = true
				self.participantsStackView.isHidden = true
			case .owner:
				self.limitationView.isHidden = true
				fallthrough
			case .admin:
				break
			}
		}
		
	}
	
	private func setupParticipants(_ participants: [Participant]) {
		DispatchQueue.main.async {
			
			self.participantsStackView.isHidden = participants.count == 0
			self.participantsCountLabel.text = "Participants" + "(\(participants.count))"
			
			let participantsPreview = participants.prefix(5)
			
			self.participantViews.enumerated().forEach({ (i,element) in
				guard i < participantsPreview.count else {
					element.isHidden = true
					return
				}
				element.setupParticipant(participantsPreview[i])
			})
		}
		
	}
	
	func setupMapView(_ location: CLLocation?) {
		
		guard let location = location else {
			self.mapView.isHidden = true
			return
		}
		
		let camera = GMSCameraPosition.camera(withTarget: location.coordinate, zoom: 15)
		self.mapView.camera = camera
		
		let marker = GMSMarker()
		marker.position = location.coordinate
		marker.map = self.mapView
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
}

extension LockDetailsViewController : ASWThemeProtocol {
	
	func setupTheme(_ theme: Theme) {
		switch theme {
		case .black:
			UIVisualEffectView.appearance(whenContainedInInstancesOf: [UIAlertController.classForCoder() as! UIAppearanceContainer.Type]).effect = UIBlurEffect(style: .light)
		default:
			break
		}
	}
	
	
	
}
