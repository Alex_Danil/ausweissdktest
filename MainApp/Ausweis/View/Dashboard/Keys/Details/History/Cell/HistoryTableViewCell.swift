//
//  HistoryTableViewCell.swift
//  Ausweis
//
//  Created by admin1 on 7/19/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import SDWebImage

class HistoryTableViewCell: UITableViewCell {
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var lastUsedLabel: UILabel!
	@IBOutlet weak var photoImageView: UIImageView!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func fill(date: String, photoURL: String, name: String) {
		photoImageView.sd_setImage(with: URL(string: photoURL), completed: nil)
		lastUsedLabel.text = date
		nameLabel.text = name
	}
}
