//
//  HistoryTableViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/19/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HistoryTableViewController: UITableViewController , NibViewModelProtocol {
	
	typealias ViewModel = LockViewModel
	var viewModel: ViewModel!
	
	let disposeBag = DisposeBag()
	
	required init(nibName nn: String, bundle: Bundle!, viewModel: ViewModel) {
		super.init(nibName: nn, bundle: bundle)
		self.viewModel = viewModel
		
		self.title = "Activity"
	
		tableView.register(UINib(nibName: HistoryTableViewCell.identifier, bundle: nil) , forCellReuseIdentifier: HistoryTableViewCell.identifier)
		tableView.dataSource = nil
		tableView.delegate = nil
		tableView.rx.setDelegate(self).disposed(by: disposeBag)
		
		observe(viewModel: viewModel)
		
		setupTheme(.black)
	}
	
	func observe(viewModel: ViewModel) {
		viewModel.input.loadLockHistory.onNext(0)
		
	
		viewModel.output.lockHistory
			.bind(to:tableView.rx.items(cellIdentifier: HistoryTableViewCell.identifier,
																	cellType: HistoryTableViewCell.self )) { row, element, cell in
																		let vm = element.historyCellViewModel()
																		cell.fill(date: vm.date, photoURL: vm.photoURL ?? "", name: vm.name ?? "No name" )
																		
			}
			.disposed(by: disposeBag)
		
	}
	
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		// self.navigationItem.rightBarButtonItem = self.editButtonItem
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	// MARK: - Table view data source
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 56
	}
	
}

extension HistoryTableViewController : ASWThemeProtocol {
	func setupTheme(_ theme: Theme) {
		switch theme {
		case .black:
			tableView.separatorColor = .black
			self.tableView.backgroundColor = .black
			self.view.backgroundColor = .black
			self.navigationController?.navigationBar.barStyle = theme.barStyle
			self.navigationController?.navigationBar.isTranslucent = false
			self.navigationController?.navigationBar.backgroundColor = .black
			self.navigationController?.navigationBar.tintColor = .white
		default: break
		}
	}
}
