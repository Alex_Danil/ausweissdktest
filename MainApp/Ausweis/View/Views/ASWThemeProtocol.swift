

//
//  ФЫЦ.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 6/15/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

protocol ASWThemeProtocol {
	func setupTheme(_ theme: Theme)
}


extension ASWThemeProtocol where Self : UIViewController {
	
	func setupTheme(_ theme: Theme) {
		self.view.backgroundColor = theme.backgroundColor
	}
	
}
