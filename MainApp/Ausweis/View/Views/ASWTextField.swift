//
//  ASWTextField.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 6/15/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

class ASWTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
	
    */
	
	override func draw(_ rect: CGRect) {
		super.draw(rect)
		
		self.layer.addBorder(edge: .bottom, color: .textBlack, thickness: 1)
	}
	
	override func awakeFromNib() {
		super.awakeFromNib()
		
		attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedStringKey.foregroundColor: UIColor.textBlack])
	}

}
