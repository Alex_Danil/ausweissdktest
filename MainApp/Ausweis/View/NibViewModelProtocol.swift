//
//  NibViewModelProtocol.swift
//  Ausweis
//
//  Created by admin1 on 7/3/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

protocol NibViewModelProtocol : class where Self: UIViewController {
	
	associatedtype ViewModel
	
	var viewModel : ViewModel! {get set}
	
	init(nibName nn:String, bundle: Bundle!, viewModel: ViewModel)
	
	func addViewModel(_ viewModel: ViewModel) -> UIViewController
}


extension NibViewModelProtocol where Self: UIViewController {
	
	func addViewModel(_ viewModel: ViewModel) -> UIViewController {
		self.viewModel = viewModel
		
		return self
	}

	
}
