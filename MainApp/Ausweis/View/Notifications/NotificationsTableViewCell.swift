//
//  NotificationsTableViewCell.swift
//  Ausweis
//
//  Created by admin1 on 7/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationsTableViewCell: UITableViewCell {

	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var bodyLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var avatarImageView: UIImageView!
	
	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setup(date: String?, photoURL: String?, text: String?, title: String?) {
		titleLabel.text = title
		bodyLabel.text = text
		
		avatarImageView.sd_setImage(with: URL(string: photoURL ?? "") , completed: nil)
		dateLabel.text = date
	}

}
