//
//  NotificationsTableViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import KRPullLoader

class NotificationsTableViewController: UITableViewController, NibViewModelProtocol, ASWThemeProtocol {
	
	var dataSource : RxTableViewSectionedReloadDataSource<NotificationsSection>!
	
	var viewModel: NotificationsViewModel!
	
	let disposeBag = DisposeBag()
	
	private var loadMoreCompletion: (() -> Void)?
	
	required init(nibName nn: String, bundle: Bundle!, viewModel: NotificationsViewModel) {
		super.init(nibName: nn, bundle: bundle)
		self.viewModel = viewModel
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let service = NotificationsService()
		viewModel = NotificationsViewModel(service: service)
		
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.estimatedRowHeight = 86
		
		setupTheme(.black)
		self.title = "Notifications"
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		viewModel.input.loadNotifications.onNext(0)
		
		tableView.dataSource = nil
		tableView.delegate = nil
		tableView.rx.setDelegate(self).disposed(by: disposeBag)
		
		dataSource = RxTableViewSectionedReloadDataSource<NotificationsSection>(configureCell: { (ds, tv, ip, notification) -> UITableViewCell in
			let cell = tv.dequeueReusableCell(withIdentifier: NotificationsTableViewCell.identifier, for: ip) as! NotificationsTableViewCell
			let vm = notification.cellViewModel()
			cell.setup(date: vm.date, photoURL: vm.photoURL, text: vm.text, title: vm.title)
			return cell
		})
		
		viewModel.output.notifications.throttle(0.7, scheduler: MainScheduler.asyncInstance)
			.bind { _ in
				DispatchQueue.main.async {
						self.loadMoreCompletion?()
				}
			}
			.disposed(by: disposeBag)
		viewModel.output.notificationsSource
			.bind(to: tableView.rx.items(dataSource: dataSource))
			.disposed(by: disposeBag)
		
		let bottomPull = KRPullLoadView()
		bottomPull.activityIndicator.activityIndicatorViewStyle = .whiteLarge
		bottomPull.delegate = self
		tableView.addPullLoadableView(bottomPull, type: .loadMore)
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	
	// MARK: - Table view data source

	
	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		
		guard let models = dataSource?.sectionModels, section < models.count + 1 else {
			return UIView()
		}
		let headerLabel = UILabel()
		headerLabel.frame.size.height = section == 0 ? 52 : 28
		headerLabel.text = "    \(dataSource[section].header)"
		headerLabel.textColor = UIColor.lightGray
		headerLabel.font = UIFont.systemFont(ofSize: 14)
		headerLabel.backgroundColor = .black
		
		return headerLabel
	}
	
	override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		let headerView = UIView()
		headerView.frame.size.height = 24
		
		return headerView
	}
	
}

extension NotificationsTableViewController : KRPullLoadViewDelegate {
	
	func pullLoadView(_ pullLoadView: KRPullLoadView, didChangeState state: KRPullLoaderState, viewType type: KRPullLoaderType) {
		switch state {
		case .loading(let completion):
			loadMoreCompletion = completion
			viewModel.input.loadNotifications.onNext(20)
		default:
			break
		}
	}
	
}

extension NotificationsTableViewController {
	
	func setupTheme(_ theme: Theme) {
		switch theme {
		case .black:
				tableView.separatorColor = .black
				self.tableView.backgroundColor = .black
				self.view.backgroundColor = .black
				self.navigationController?.navigationBar.barStyle = theme.barStyle
				self.navigationController?.navigationBar.isTranslucent = false
				self.navigationController?.navigationBar.backgroundColor = .black
				self.navigationController?.navigationBar.tintColor = .white
		default: return
		}
		
	}
	
}
