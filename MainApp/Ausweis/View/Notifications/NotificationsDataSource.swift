//
//  NotificationsDataSource.swift
//  Ausweis
//
//  Created by admin1 on 7/24/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import RxDataSources
import Ausweis_ioSDK

struct NotificationsSection {
	var header: String
	var items: [Item]
}
extension NotificationsSection: SectionModelType {
	init(original: NotificationsSection, items: [LockNotification]) {
		self = original
		self.items = items
	}

	
	typealias Item = LockNotification
	
	init(title: String, items: [Item]) {
		self.header = title
		self.items = items
	}
}
