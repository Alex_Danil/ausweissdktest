//
//  ProfileTableViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/18/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import SDWebImage

class ProfileTableViewController: UITableViewController {
	
	@IBOutlet weak var profileImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var emailLabel: UILabel!
	@IBOutlet weak var phoneLabel: UILabel!
	
	@IBOutlet weak var notificationSwitch: UISwitch!
	@IBOutlet weak var emailNotificationsSwitch: UISwitch!
	
	private var viewModel: UserViewModel!
	
	private let disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.title = "Profile"
		setupTheme(.black)
		
		let service = UserService()
		
		viewModel = UserViewModel(service: service)
		observe(viewModel: viewModel)
	}
	
	func observe(viewModel: UserViewModel) {
		viewModel.output.email
			.bind(to: emailLabel.rx.text)
			.disposed(by: disposeBag)
		
		viewModel.output.phone
			.bind(to: phoneLabel.rx.text)
			.disposed(by: disposeBag)
		
		viewModel.output.name
			.bind(to: nameLabel.rx.text)
			.disposed(by: disposeBag)
		
	
		viewModel.output.photo.bind { url in
			
			self.profileImageView.sd_setImage(with: url, completed: nil)
			
		}.disposed(by: disposeBag)
		
		viewModel.output.isNotificationsEnabled
			.bind(to: notificationSwitch.rx.isOn)
			.disposed(by: disposeBag)
		
		viewModel.output.isEmailNotificationsEnabled
			.bind(to: emailNotificationsSwitch.rx.isOn)
			.disposed(by: disposeBag)
		
		emailNotificationsSwitch.rx.value.skip(1)
			.map { $0 }
			.subscribe(viewModel.input.emailNotifications)
			.disposed(by: disposeBag)
		
		notificationSwitch.rx.value.skip(1)
			.map { $0 }
			.subscribe(viewModel.input.notifications)
			.disposed(by: disposeBag)
		
		let loadingVC = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
		
		viewModel.isLoading.observeOn(MainScheduler.asyncInstance)
			.bind(onNext: { loading in
				if loading {
					self.add(loadingVC)
				} else {
					loadingVC.remove()
				}
			})
			.disposed(by: disposeBag)
		
	}
	
	@IBAction func logoutTapped(_ sender: Any) {
		viewModel.logout.onNext(())
		LoginRouter.logout()
	}
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
}

extension ProfileTableViewController {
	
	override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
		return UIView()
	}
	
}

extension ProfileTableViewController : ASWThemeProtocol {
	
	func setupTheme(_ theme: Theme) {
		switch theme {
		case .black:
			self.tableView.backgroundColor = .black
			self.view.backgroundColor = .black
			self.navigationController?.navigationBar.barStyle = theme.barStyle
			self.navigationController?.navigationBar.isTranslucent = false
			self.navigationController?.navigationBar.backgroundColor = .black
			self.navigationController?.navigationBar.tintColor = .white
		default:
			return
		}
	}
}




extension ProfileTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
	
	@IBAction func addPhotoTapped(_ sender: Any) {
		
		let alertController = UIAlertController(title: "Select photo source", message: nil, preferredStyle: .actionSheet)
		let photo = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: {(action) -> Void in
			self.selectPhoto(withSource: .library)
		})
		
		let camera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: {(action) -> Void in
			self.selectPhoto(withSource: .camera)
		})
		
		alertController.addAction(camera)
		alertController.addAction(photo)
		self.present(alertController, animated: true, completion: nil)
	}
	func selectPhoto (withSource src: PhotoSource ) {
		
		let vc = UIImagePickerController()
		vc.delegate = self
		
		switch src {
		case .camera:
			vc.sourceType = .camera
			vc.cameraCaptureMode = .photo
			vc.allowsEditing = true
		case .library:
			vc.allowsEditing = true
			vc.sourceType = .photoLibrary
			vc.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
		}
		
		present(vc, animated: true)
	}
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		picker.dismiss(animated: true)
		
		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage, let path = image.save(named: "avatar") else {
			print("No image found")
			return
		}
		
		profileImageView.image = image

		viewModel.input.photoPath.onNext(path)
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
	}
	
}
