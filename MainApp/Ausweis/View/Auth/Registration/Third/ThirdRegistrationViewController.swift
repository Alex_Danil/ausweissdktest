//
//  ThirdRegistrationViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift

class ThirdRegistrationViewController: UIViewController, RegisterFlow {
	
	@IBOutlet weak var bottomSeparatorView: SeparatorView!
	@IBOutlet weak var countryImageView: UIImageView!
	@IBOutlet weak var phoneExtensionLabel: UILabel!
	@IBOutlet weak var selectCountryButton: UIButton!
	@IBOutlet weak var phoneTextField: TextFieldPatternFormat!
	
	private var country: Country = Country(countryCode: "UA", phoneExtension: "380") {
		didSet {
			setupCountry()
		}
	}
	
	var nextVC: (() -> ())!
	var prevVC: (() -> ())!
	
	var viewModel: UserRegisterViewModel!
	
	let disposeBag = DisposeBag()
	
	private let selectCountryVC: CountriesViewController = CountriesViewController.standardController()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		selectCountryVC.delegate = self
		setupCountry()
		
		
		phoneTextField.rx.text.asObservable()
			.ignoreNil()
			.map {
				return self.phoneExtensionLabel!.text! + $0
			}
			.bind(to: viewModel.input.phone)
			.disposed(by: disposeBag)
		
		// Do any additional setup after loading the view.
	}
	
	private func setupCountry() {
		let source = NKVSource(country: country)
		
		countryImageView.image = NKVSourcesHelper.flag(for: source)
		phoneExtensionLabel.text = "+\(country.phoneExtension)"
		selectCountryButton.setTitle(country.name, for: .normal)
		var realFormatting = country.formatPattern
		for _ in country.phoneExtension.characters {
			realFormatting = String(realFormatting.dropFirst())
		}
	
		phoneTextField.setFormatting(realFormatting, replacementChar: "#")
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func countryButtonTapped(_ sender: Any) {
		self.present(selectCountryVC, animated: true, completion: nil)
	}
	
	@IBAction func nextButtonTapped(_ sender: Any) {
		guard let phone = phoneTextField!.text, !phone.isEmpty else {
			LoginRouter.showAlert(message: "PHONE_EMPTY".uppercased().localized, fromVC: self)
			return
		}
		
		nextVC()
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}

extension ThirdRegistrationViewController : CountriesViewControllerDelegate {
	
	func countriesViewController(_ sender: CountriesViewController, didSelectCountry country: Country) {
		self.country = country
		selectCountryVC.dismiss(animated: true, completion: nil)
	}
	
	func countriesViewControllerDidCancel(_ sender: CountriesViewController) {
		selectCountryVC.dismiss(animated: true, completion: nil)
	}
	
}

class SeparatorView: UIView {
	
	override func draw(_ rect: CGRect) {
		let path = UIBezierPath()
		path.move(to:CGPoint(x: 0, y: 0))
		path.addLine(to: CGPoint(x: 25, y: 0))
		path.addLine(to:CGPoint(x: 30, y: 5))
		path.addLine(to:CGPoint(x: 35, y: 0))
		path.addLine(to:CGPoint(x: rect.width, y: 0))
		path.lineWidth = 1
		path.close()
		
		UIColor.textBlack.set()
		
		path.fill()
		
		self.layer.addBorder(edge: .top, color: .textBlack, thickness: 1)
	}
	
}

extension ThirdRegistrationViewController : UITextFieldDelegate {
	
}

