//
//  SixthViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/22/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import Ausweis_ioSDK

class SixthViewController: UIViewController, RegisterFlow {
	
	var nextVC: (() -> ())!
	var prevVC: (() -> ())!
	
	var viewModel: UserRegisterViewModel!
	
	@IBOutlet weak var firstDigitField: UITextField!
	@IBOutlet weak var secondDigitField: UITextField!
	@IBOutlet weak var thirdDigitField: UITextField!
	@IBOutlet weak var fourthDigitField: UITextField!
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		
		
		// Do any additional setup after loading the view.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func nextButtonTapped(_ sender: Any) {
		
		guard let first = firstDigitField.text, !first.isEmpty, let second = secondDigitField.text, !second.isEmpty, let third = thirdDigitField.text, !third.isEmpty, let fourth = fourthDigitField.text, !fourth.isEmpty else {
			LoginRouter.showAlert(message: "VERIFICATION_EMPTY".localized, fromVC: self)
			return
		}
		
		ASWUserSDK().confirmCode("\(first)\(second)\(third)\(fourth)", completion: { _ in
			print("Confirmed")
			self.nextVC()
		}) { (error) in
			LoginRouter.showAlert(message: error?.errors.first?.message ?? "Undefined errors", fromVC: self)
		}
		nextVC()
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}


extension SixthViewController : UITextFieldDelegate {
	
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		
		if textField.text!.count < 1  && string.count > 0{
			let nextTag = textField.tag + 1
			
			// get next responder
			var nextResponder = textField.superview?.viewWithTag(nextTag)
			
			if nextResponder == nil {
				
				if textField.tag == 3 {
					textField.resignFirstResponder()
				} else {
					nextResponder = textField.superview?.viewWithTag(1)
				}
				
			}
			textField.text = string
			nextResponder?.becomeFirstResponder()
			return false
		}
		else if textField.text!.count >= 1  && string.count == 1{
			return false
		}
		return true
		
	}
	
}
