//
//  FirstRegistratiomViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FirstRegistrationViewController: UIViewController, RegisterFlow {
	
	var nextVC: (() -> ())!
	var prevVC: (() -> ())!
	
	@IBOutlet weak var firstNameField: ASWTextField!
	@IBOutlet weak var secondNameField: ASWTextField!
	@IBOutlet weak var nextButton: UIButton!
	
	var viewModel: UserRegisterViewModel!
	
	let disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.backgroundColor = Theme.black.loginBackgroundColor
		
		secondNameField.rx.text.asObservable()
			.ignoreNil()			
			.subscribe(viewModel.input.lastName)
			.disposed(by: disposeBag)
		
		
		firstNameField.rx.text.asObservable()
			.ignoreNil()
			.subscribe(viewModel.input.firstName)
			.disposed(by: disposeBag)
		
		// Do any additional setup after loading the view.
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.view.backgroundColor = .clear
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func nextButtonTapped(_ sender: Any) {
		
		guard let name = firstNameField!.text, let surname = secondNameField!.text , !name.isEmpty, !surname.isEmpty  else {
			LoginRouter.showAlert(message: "NAME_EMPTY".uppercased().localized, fromVC: self)
			return
		}
		nextVC?()
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
