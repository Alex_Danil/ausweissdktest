//
//  FourthRegisterViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/21/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FourthRegisterViewController: UIViewController, RegisterFlow , UINavigationControllerDelegate
{
	
	var nextVC: (() -> ())!
	var prevVC: (() -> ())!
	
	@IBOutlet weak var passwordTextField: ASWTextField!
	@IBOutlet weak var confirmTextField: ASWTextField!
	@IBOutlet weak var nextButton: UIButton!
	
	var viewModel: UserRegisterViewModel!
	
	let disposeBag = DisposeBag()
	
	private let loadVC = LoginRouter.loadVC

	override func viewDidLoad() {
		super.viewDidLoad()
		
		passwordTextField.rx.text.asObservable()
			.ignoreNil()
			.bind(to: viewModel.input.password)
			.disposed(by: disposeBag)
		
		viewModel.output.registerResultObservable
			.observeOn(MainScheduler.asyncInstance)
			.subscribe { [weak self] (success) in
					self?.loadVC.remove()
					self?.nextVC()
			}
			.disposed(by: disposeBag)
		
		viewModel.output.errorsObservable
			.observeOn(MainScheduler.asyncInstance)
			.subscribe(onNext: { [weak self] error in
				self?.loadVC.remove()
			})
			.disposed(by: disposeBag)
		
		
		nextButton.rx.tap.asObservable().subscribeOn(MainScheduler.asyncInstance)
			.subscribe(onNext: { [weak self, unowned loadVC] in
				
				guard let pass = self?.passwordTextField!.text, let confirm = self?.confirmTextField!.text , !pass.isEmpty, !confirm.isEmpty  else {
					LoginRouter.showAlert(message: "PASSWORD_EMPTY".uppercased().localized, fromVC: self)
					self?.loadVC.remove()
					return
				}
				
				if self?.passwordTextField!.text != self?.confirmTextField!.text {
					LoginRouter.showAlert(message: "PASSWORDS_NOT_EQUAL".uppercased().localized, fromVC: self)
				} else {
					self?.add(loadVC)
					self?.viewModel.input.registerDidTap.onNext(())
				}
			})
			.disposed(by: disposeBag)

		
		
		// Do any additional setup after loading the view.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func showPasswordTapped(_ sender: Any) {
		passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
	}
	
	@IBAction func showConfirmPasswordTapped(_ sender: Any) {
		confirmTextField.isSecureTextEntry = !confirmTextField.isSecureTextEntry
	}
	
	@IBAction func nextButtonTapped(_ sender: Any) {
		
		
		
	}
	
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
