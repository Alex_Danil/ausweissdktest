//
//  RegistrationPageViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift

class RegistrationPageViewController: UIPageViewController {
	
	private var pages : Array<UIViewController & RegisterFlow>!
	
	private var currentIndex : Int = 0 
	private var titles = ["Enter your name", "Enter your e-mail", "Enter your phone", "Add photo", "Enter your password", "Confirmation code"]
	private let stepLabel = UILabel()
	private var viewModel: UserRegisterViewModel!
	private let disposeBag = DisposeBag()
	private	let registerService = RegisterService()
	
	override func viewDidLoad() {
	
		viewModel = UserRegisterViewModel(registerService)
		let firstVC 	= FirstRegistrationViewController(nibName: "FirstRegistrationViewController", viewModel: viewModel ,  next: self.nextVC, prev: self.prevVC)
		let secondVC 	= SecondRegistrationViewController(nibName: "SecondRegistrationViewController", viewModel: viewModel ,  next: self.nextVC, prev: self.prevVC)
		let thirdVC 	= ThirdRegistrationViewController(nibName: "ThirdRegistrationViewController", viewModel: viewModel, next: self.nextVC, prev: self.prevVC)
		let fourthVC 	= FourthRegisterViewController(nibName: "FourthRegisterViewController", viewModel: viewModel,  next: self.nextVC, prev: self.prevVC)
		let fifthVC 	= FifthRegisterViewController(nibName: "FifthRegisterViewController", viewModel: viewModel,  next: self.nextVC, prev: self.prevVC)
		let sixthVC 	= SixthViewController(nibName: "SixthViewController", viewModel: viewModel, next: self.nextVC, prev: self.prevVC)
		
		pages = [firstVC, secondVC, thirdVC, fifthVC, fourthVC, sixthVC]
		
		super.viewDidLoad()
		self.delegate = self
		addStepperView()
		view.backgroundColor = Theme.black.loginBackgroundColor

		if let firstVC = pages.first {
			setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
		}
		
		let customBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "backArrow"), style: .plain, target: self, action: #selector(backAction(_:)))
		customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
		navigationItem.leftBarButtonItem = customBackButton
		
		
		viewModel.output.errorsObservable.subscribe(onNext:{ (error) in
			LoginRouter.showAlert(message: error.errors.first?.message, fromVC: self)
		}).disposed(by: disposeBag)
		
		// Do any additional setup after loading the view.
	}
	
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}
	
	func addStepperView () {
		let stepBackgroundView = UIView()
		view.addSubview(stepBackgroundView)
		
		stepBackgroundView.backgroundColor = Theme.black.cellBackground
		stepBackgroundView.translatesAutoresizingMaskIntoConstraints = false

		stepBackgroundView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
		stepBackgroundView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
		stepBackgroundView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
		stepBackgroundView.heightAnchor.constraint(equalToConstant: 48).isActive = true
		
		stepBackgroundView.addSubview(stepLabel)
		
		stepLabel.translatesAutoresizingMaskIntoConstraints = false
		stepLabel.leftAnchor.constraint(equalTo: stepBackgroundView.leftAnchor, constant: 8).isActive = true
		stepLabel.bottomAnchor.constraint(equalTo: stepBackgroundView.bottomAnchor, constant: -8).isActive = true
		
		stepLabel.text = "Step 1 of 6".uppercased()
		stepLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
		stepLabel.textColor = Theme.black.titleLabelColor
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	@objc func backAction(_ sender: Any) {
		if currentIndex == 0 {
			self.navigationController?.popViewController(animated: true)
		} else {
			prevVC()
		}
	}

	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}

extension RegistrationPageViewController : UIPageViewControllerDelegate {
	
	func nextVC () {
		if currentIndex >= pages.count - 1 {
			currentIndex = pages.count - 1
			DashboardRouter().route(to: .mainTabBar , from: self, parameters: nil)
			return
		} else {
			currentIndex = currentIndex + 1
		}
		DispatchQueue.main.async {
			self.setController(atIndex: self.currentIndex, direction: .forward)
		}
	}

	func prevVC () {
		if currentIndex <= 0 {
			currentIndex = 0
		} else {
			currentIndex = currentIndex - 1
		}
		DispatchQueue.main.async {
			self.setController(atIndex: self.currentIndex, direction: .reverse)
		}
	}
	
	private func setController(atIndex ix: Int , direction:UIPageViewControllerNavigationDirection) {
			setViewControllers([pages[currentIndex]], direction: direction, animated: true, completion: nil)
			title = titles[currentIndex]
			stepLabel.text = "Step \(currentIndex + 1) of 6".uppercased()
	}
	
	func presentationCount(for pageViewController: UIPageViewController) -> Int {
		return pages.count
	}
	
	func presentationIndex(for pageViewController: UIPageViewController) -> Int {
		return 0
	}
	
}

extension RegistrationPageViewController : ASWThemeProtocol {
	func setupTheme(_ theme: Theme) {
		self.view.backgroundColor = theme.loginBackgroundColor
		self.navigationController?.navigationBar.barStyle = theme.barStyle
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
}
