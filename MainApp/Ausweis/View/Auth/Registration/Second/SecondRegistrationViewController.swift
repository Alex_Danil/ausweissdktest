//
//  SecondRegistrationViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SecondRegistrationViewController: UIViewController, RegisterFlow {
	
	var nextVC: (() -> ())!
	var prevVC: (() -> ())!
	
	@IBOutlet weak var emailTextField: ASWTextField!
	
	var viewModel: UserRegisterViewModel!
	
	let disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		emailTextField.rx.text.asObservable()
			.ignoreNil()
			.subscribe(viewModel.input.email)
			.disposed(by: disposeBag)
		
		viewModel.output.checkEmailResultObservable
			.observeOn(MainScheduler.asyncInstance)
			.subscribe { (success) in			
					self.nextVC()
			}.disposed(by: disposeBag)
		
		// Do any additional setup after loading the view.
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func nextTapped(_ sender: Any) {
		
		guard let email = emailTextField.text, !email.isEmpty else {
			LoginRouter.showAlert(message: "EMAIL_EMPTY".uppercased().localized, fromVC: self)
			return
		}
		
		viewModel.input.checkEmailDidTap.onNext(())
	}
	
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	
}
