//
//  RegisterFlowProtocol.swift
//  Ausweis
//
//  Created by admin1 on 6/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit


protocol RegisterFlow {
	
	var nextVC : (()->())! {get set}
	var prevVC : (()->())! {get set}
	
	var viewModel : UserRegisterViewModel! {get set}
	
}


extension RegisterFlow where Self : UIViewController {
	
	init(nibName nn:String, bundle: Bundle! = nil, viewModel: UserRegisterViewModel, next: @escaping (()->()), prev: @escaping (()->()) ) {
		self.init(nibName: nn, bundle: bundle)
		
		nextVC = next
		prevVC = prev
		
		self.viewModel = viewModel
	}
	
}
