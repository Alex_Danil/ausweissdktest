//
//  FifthRegisterViewController.swift
//  Ausweis
//
//  Created by admin1 on 6/21/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import RxSwift

enum PhotoSource {
	case camera
	case library
}

class FifthRegisterViewController: UIViewController, UINavigationControllerDelegate, RegisterFlow {
	
	var nextVC: (() -> ())!
	var prevVC: (() -> ())!
	
	@IBOutlet weak var selectPhotoButton: UIButton!
	@IBOutlet weak var photoImageView: UIImageView!
	@IBOutlet weak var nextButton: UIButton!
	
	var viewModel: UserRegisterViewModel!

	let disposeBag = DisposeBag()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Do any additional setup after loading the view.
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		nextButton.isEnabled = true
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func addPhotoTapped(_ sender: Any) {
		
		let alertController = UIAlertController(title: "Select photo source", message: nil, preferredStyle: .actionSheet)
		let photo = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default, handler: {(action) -> Void in
			self.selectPhoto(withSource: .library)
		})
		
		let camera = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default, handler: {(action) -> Void in
			self.selectPhoto(withSource: .camera)
		})
		
		alertController.addAction(camera)
		alertController.addAction(photo)
		self.present(alertController, animated: true, completion: nil)
	}
	
	
	@IBAction func nextButtonTapped(_ sender: Any) {
		nextButton.isEnabled = false
		nextVC()
	}
	
	func selectPhoto (withSource src: PhotoSource ) {
		
		let vc = UIImagePickerController()
		vc.delegate = self
		
		switch src {
		case .camera:
			vc.sourceType = .camera
			vc.cameraCaptureMode = .photo
			vc.allowsEditing = true
		case .library:
			vc.allowsEditing = true
			vc.sourceType = .photoLibrary
			vc.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
		}
		
		present(vc, animated: true)
	}
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/

}

extension FifthRegisterViewController: UIImagePickerControllerDelegate {
	
	func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
		picker.dismiss(animated: true)
		
		guard let image = info[UIImagePickerControllerEditedImage] as? UIImage, let path = image.save(named: "avatar") else {
			print("No image found")
			return
		}
		
		selectPhotoButton.setImage(image, for: .normal)
		viewModel.input.photoPath.onNext(path)
	}
	
	func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
		dismiss(animated: true, completion: nil)
	}
	
}
