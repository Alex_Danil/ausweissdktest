//
//  LoginScreenViewController.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 4/24/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import Ausweis_ioSDK
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit

class LoginScreenViewController: UIViewController {
	
	@IBOutlet weak var loginButton: UIButton!
	@IBOutlet weak var emailTextField: ASWTextField!
	@IBOutlet weak var passwordTextField: ASWTextField!
	
	@IBOutlet weak var googleSignIn: GIDSignInButton!
	@IBOutlet weak var facebookSignIn: UIButton!
	
	
	private let auth = ASWUserSDK()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Log in"
		
		setupTheme(.black)
		
		GIDSignIn.sharedInstance().uiDelegate = self
		GIDSignIn.sharedInstance().delegate = self

	}
	@IBAction func viewTapped(_ sender: Any) {
		view.endEditing(true)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		refreshToken()
	}

	
	@IBAction func loginButtonTapped(_ sender: Any) {
		view.endEditing(true)
		
		let emailValidated = ValidationManager.validateEmail(emailTextField.text)
		
		guard emailValidated.success else { LoginRouter.showAlert(message: emailValidated.errorString, fromVC: self); return }
		
		auth.login(emailTextField.text ?? "", password: passwordTextField.text ?? "", completion: { (user) in
			print("USER \(user.debugDescription)")
			
			DispatchQueue.main.async {
				RealmManager.shared.createOrUpdate(model: user, with: { user in
					return UserObject(user: user!)
				})
				
				DashboardRouter().route(to: .mainTabBar, from: self, parameters: nil)
			}
		}) { (error) in
			LoginRouter.showAlert(message: error?.errors.first?.message , fromVC: self)
		}
	}
	
	@IBAction func registerButtonTapped(_ sender: Any) {
		LoginRouter().route(to: .signUp, from: self, parameters: nil)
	}

	@IBAction func googleSignInTapped(_ sender: Any) {
		GIDSignIn.sharedInstance().signIn()
	}
	
	
	@IBAction func facebookSignInTapped(_ sender: Any) {
		
		let fb = FBSDKLoginManager()
		
		fb.logIn(withPublishPermissions: nil, from: self) { (result, error) in
			guard let token = result?.token  else {
                
				return
			}
			
			self.loginSocial(.facebook, token: token.tokenString)
		}
		
	}
	
	@IBAction func showPassword(_ sender: Any) {
		passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
	}
	
	func refreshToken() {
		
		let loadingVC = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
		
		add(loadingVC)
		
		auth.refreshToken({ (success) in
			DispatchQueue.main.async {
				loadingVC.remove()
				DashboardRouter().route(to: .mainTabBar, from: self, parameters: nil)
			}
		}) { (error) in
			DispatchQueue.main.async {
				loadingVC.remove()
			}
			
			print(error)
		}
	}
	
}

extension LoginScreenViewController : UITextFieldDelegate {
	
	func textFieldShouldReturn(_ textField: UITextField) -> Bool {
		
		switch textField.tag {
		case 0:
			passwordTextField.becomeFirstResponder()
		case 1:
			passwordTextField.resignFirstResponder()
			loginButtonTapped(textField)
		default:
			break
		}
		
		return true
	}
	
}


extension LoginScreenViewController: ASWThemeProtocol {
	
	func setupTheme(_ theme: Theme) {
		self.view.backgroundColor = theme.loginBackgroundColor
		self.navigationController?.navigationBar.barStyle = theme.barStyle
		self.navigationController?.navigationBar.isTranslucent = false
		self.navigationController?.navigationBar.backgroundColor = .black
		self.navigationController?.navigationBar.tintColor = .white
	}
	
}

extension LoginScreenViewController : GIDSignInUIDelegate {
	
	// Implement these methods only if the GIDSignInUIDelegate is not a subclass of
	// UIViewController.
	
	// Stop the UIActivityIndicatorView animation that was started when the user
	// pressed the Sign In button
	func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
//		myActivityIndicator.stopAnimating()
	}
	
	// Present a view that prompts the user to sign in with Google
	func sign(_ signIn: GIDSignIn!,
						present viewController: UIViewController!) {
		present(viewController, animated: true, completion: nil)
	}
	
	// Dismiss the "Sign in with Google" view
	func sign(_ signIn: GIDSignIn!,
						dismiss viewController: UIViewController!) {
		dismiss(animated: true, completion: nil)
	}
	
	
}


extension LoginScreenViewController : GIDSignInDelegate {
	func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
		if let error = error {
			print("\(error.localizedDescription)")
		} else {
			// Perform any operations on signed in user here.
			guard let idToken = user.authentication.accessToken else {
				print("NO GOOGLE TOKEN")
				return
			} // Safe to send to the server
			
			self.loginSocial(.google, token: idToken)
			
		}
	}
}

extension LoginScreenViewController {
	
	func loginSocial(_ social: ThirdPartyAuth, token: String) {
		
		let loadingVC = LoadingViewController(nibName: "LoadingViewController", bundle: nil)
		
		add(loadingVC)
		
		auth.loginSocial(social, token: token , completion: { (user) in
			DispatchQueue.main.async {
				loadingVC.remove()

				RealmManager.shared.createOrUpdate(model: user, with: { user in
					return UserObject(user: user!)
				})
				
				DashboardRouter().route(to: .mainTabBar, from: self, parameters: nil)
			}
		}) { error in
			loadingVC.remove()

			print(error)
		}
	}
}

//extension LoginScreenViewController : FBSDKApplicationDelegate {
//
//}


