//
//  Theme.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 6/15/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

enum Theme {
	
	case white, black
	
	var backgroundColor : UIColor {
		switch self {
		case .white :
			return UIColor.mainWhite
		case .black :
			return UIColor.mainBlack
		}
	}
	
	var titleLabelColor : UIColor {
		switch self {
		case .white :
			return UIColor.titleWhite
		case .black :
			return UIColor.titleBlack
		}
	}
	
	var textLabelColor : UIColor {
		switch self {
		case .white :
			return UIColor.textWhite
		case .black :
			return UIColor.textBlack
		}
	}
	
	var loginBackgroundColor : UIColor {
		switch self {
		case .white :
			return UIColor.textWhite
		case .black :
			return UIColor.blackThree
		}
	}
	
	var barStyle: UIBarStyle {
		switch self {
		case .white:
			return .default
		case .black:
			return .black
		}
	}
	
	var cellBackground: UIColor {
		switch self {
		case .white:
			return .white
		case .black:
			return .cellBlack
		}
	}
	
}
