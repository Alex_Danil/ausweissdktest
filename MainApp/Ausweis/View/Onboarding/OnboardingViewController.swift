//
//  OnboardingViewController.swift
//  Ausweis
//
//  Created by admin1 on 4/10/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import Lottie

class OnboardingViewController: UIViewController {
	
	@IBOutlet weak var lotAnimationViewBackground: UIView!
	@IBOutlet weak var pageControl: UIPageControl!
	@IBOutlet weak var gotItButton: UIButton!
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var textLabel: UILabel!
	
	var lotAnimationView : LOTAnimationView!
	
	let textArray : Array<(title:String, text: String)> = [(title:"Welcome",text:"Manage your own Ausweis Device or use shared digital keys"),
																												 (title:"Unlocking",text:"Slide-to-Unlock, scan QR or use NFC Tag to open locks"),
																												 (title:"Sharing",text:"Send and revoke keys online, even for users without registration"),
																												 (title:"Schedule",text:"Use unlimited, single-use or scheduled\nkeys for each user"),
																												 (title:"Nofifications",text:"Get alert notifications, browse\nactivity log 24/7")]
	
	
	lazy var pageProgress: ()->(CGFloat) = self.calculatePageProgress
	
	
	override func awakeFromNib() {
		super.awakeFromNib()
	}
	
	
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		lotAnimationView.frame = lotAnimationViewBackground.bounds
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let rightSwipeGestureRecongizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeToRight(_:)))
		rightSwipeGestureRecongizer.direction = .right
		
		let leftSwipeGestureRecongizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeToLeft(_:)))
		leftSwipeGestureRecongizer.direction = .left
		
		lotAnimationViewBackground.addGestureRecognizer(rightSwipeGestureRecongizer)
		lotAnimationViewBackground.addGestureRecognizer(leftSwipeGestureRecongizer)

		gotItButton.layer.cornerRadius = 18
		gotItButton.layer.shadowOffset = CGSize(width: 0, height: 12)
		gotItButton.layer.shadowColor = gotItButton.backgroundColor?.cgColor
		gotItButton.layer.shadowOpacity = 1
		gotItButton.layer.shadowRadius = 24
		
		// Do any additional setup after loading the view.
	}

	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		self.navigationController?.setNavigationBarHidden(true, animated: false)
		
		lotAnimationView = LOTAnimationView(name: "Onboarding")
		
		let gradient = CAGradientLayer()
		gradient.frame = view.bounds
		gradient.colors = [	UIColor.clear.cgColor,	UIColor.black.cgColor]
		gradient.locations = [0, 1]
		gradient.startPoint = CGPoint(x: 0.5, y: 0.79)
		gradient.endPoint = CGPoint(x: 0.5, y: 0)
		lotAnimationViewBackground.layer.addSublayer(gradient)
		
		lotAnimationViewBackground.addSubview(lotAnimationView)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		lotAnimationViewBackground.isUserInteractionEnabled = false
		switchPage()
	}
	
	fileprivate func switchPage() {
		
		titleLabel.text = textArray[pageControl.currentPage].title
		textLabel.text = textArray[pageControl.currentPage].text
		
		print("PROGRESS \(pageProgress() * CGFloat(pageControl.currentPage))")
		lotAnimationView.play(toProgress: (pageProgress() * CGFloat(pageControl.currentPage) / 100.0 )) { (completed) in
			self.lotAnimationViewBackground.isUserInteractionEnabled = completed
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func calculatePageProgress() -> CGFloat {
		return 100.0 / CGFloat(pageControl.numberOfPages - 1)
	}
	
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		UserDefaults.standard.set(true, forKey: "alreadyInstalled")
		
		// Show the navigation bar on other view controllers
		self.navigationController?.setNavigationBarHidden(false, animated: animated)
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/
	@IBAction func gotItButtonTapped(_ sender: Any) {
		LoginRouter().route(to: .login, from: self, parameters: nil)
	}
	
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
}

//MARK : - SwipeGestureRecognizers

extension OnboardingViewController {
	
	@objc func swipeToRight(_ sender: Any) {
		if pageControl.currentPage != 0 {
			print("right -")
			pageControl.currentPage = pageControl.currentPage - 1
			switchPage()
		}
	}
	
	@objc func swipeToLeft(_ sender: Any) {
		print("left +")
		if pageControl.currentPage != pageControl.numberOfPages - 1 {
			pageControl.currentPage = pageControl.currentPage + 1
			switchPage()
		}
	}
	
	
}
