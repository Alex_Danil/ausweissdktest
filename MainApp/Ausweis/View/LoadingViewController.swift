//
//  LoadingViewController.swift
//  Ausweis
//
//  Created by admin1 on 7/25/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class LoadingViewController: UIViewController {
	
	@IBOutlet weak var visualEffectView: UIVisualEffectView!
	
	@IBOutlet weak var activityIndicatorView: NVActivityIndicatorView!
	
	
	override func viewDidLoad() {
		super.viewDidLoad()
		visualEffectView.alpha = 0
		// Do any additional setup after loading the view.
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		visualEffectView.center = UIWindow().center
		UIView.animate(withDuration: 0.5) {
			self.visualEffectView.alpha = 1
		}
		DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
			self.activityIndicatorView.startAnimating()
		}
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	
	override func viewWillLayoutSubviews() {
//		view.layoutIfNeeded()
	}
	
	
	/*
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
	// Get the new view controller using segue.destinationViewController.
	// Pass the selected object to the new view controller.
	}
	*/

}
