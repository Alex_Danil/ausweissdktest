//
//  LockViewModel.swift
//  Ausweis
//
//  Created by admin1 on 7/10/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import Ausweis_ioSDK
import RxSwift
import CoreLocation

class LockViewModel: ViewModelProtocol {
	
	struct Input {
		var lockID: ReplaySubject<Int>
		var loadLockDetails: AnyObserver<Void>
		var loadLockHistory: AnyObserver<Int>
	}
	
	struct Output {
		fileprivate let lock: Observable<LockResponse>
		let lockID: BehaviorSubject<Int>
		let lockName: Observable<String>
		let address: Observable<String?>
		let location: Observable<CLLocation?>
		let isNotificationsOn: Observable<Bool>
		let accessType: Observable<LockAccessType>
		let lockParticipants: Observable<[Participant]>
		
		let lockHistory: Observable<[LockHistory]>
		
		let errorsObservable: Observable<ResponseError>
	}
	
	let input: Input
	let output: Output
	
	private let lockIDSubject = ReplaySubject<Int>.create(bufferSize: 1)
	private let lockSubject = ReplaySubject<LockResponse>.create(bufferSize: 1)
	private let lockDetailsSubject = ReplaySubject<LockDetailsResponse>.create(bufferSize: 1)
	private let lockNameSubject = ReplaySubject<String>.create(bufferSize: 1)
	private let addressSubject = ReplaySubject<String?>.create(bufferSize: 1)
	private let locationSubject = ReplaySubject<CLLocation?>.create(bufferSize: 1)
	private let isNotificationsOnSubject = ReplaySubject<Bool>.create(bufferSize: 1)
	private let lockAccessTypeSubject = ReplaySubject<LockAccessType>.create(bufferSize: 1)
	
	private let lockParticipantsSubject = ReplaySubject<[Participant]>.create(bufferSize: 1)
	private let lockHistorySubject = ReplaySubject<[LockHistory]>.create(bufferSize: 1)
	
	
	private let loadLockDetailsSubject = PublishSubject<Void>()
	private let loadLockHistorySubject = PublishSubject<Int>()
	
	private let errorsSubject = PublishSubject<ResponseError>()
	
	let disposeBag = DisposeBag()
	
	init<T:LocksServiceProtocol>(keysService service: T, lock:LockResponse) {
		input = Input(lockID: lockIDSubject.asObserver(),
									loadLockDetails: loadLockDetailsSubject.asObserver(),
									loadLockHistory: loadLockHistorySubject.asObserver())
		
		output = Output(lock: lockSubject.asObserver(),
										lockID: BehaviorSubject<Int>(value: lock.ID),
										lockName: lockNameSubject.asObserver(),
										address: addressSubject.asObserver(),
										location: locationSubject.asObserver(),
										isNotificationsOn: isNotificationsOnSubject.asObserver(),
										accessType: lockAccessTypeSubject.asObserver(),
										lockParticipants: lockParticipantsSubject.asObserver(),
										lockHistory: lockHistorySubject.asObserver(),
										errorsObservable: errorsSubject.asObserver())
		
		handleService(service)
		handleLock()
		
		lockSubject.onNext(lock)
		lockIDSubject.onNext(lock.ID)
		
	}
	
	private func handleLock() {
		lockSubject.asObserver().subscribe(onNext: { (lock) in
			self.lockNameSubject.onNext(lock.name)
			self.locationSubject.onNext(lock.location)
			self.addressSubject.onNext(lock.address)
			self.isNotificationsOnSubject.onNext(lock.isActivityNotification ?? false)
			self.lockAccessTypeSubject.onNext(lock.accessType ?? .guest)
			
		}).disposed(by: disposeBag)
		
	}
	
	func handleService(_ service: LocksServiceProtocol) {
		loadLockDetailsSubject.asObserver()
			.withLatestFrom(lockIDSubject.asObservable())
			.flatMap { lockID in
				service.getLockDetails(lockID: lockID).materialize()
			}.subscribe(onNext: { (event) in
				switch event {
				case .next(let response):
					
					if response.error == nil {
						guard let lockDetails = response.lock else {
							return
						}
						
						self.lockDetailsSubject.onNext(lockDetails)
						
						self.lockNameSubject.onNext(lockDetails.lock.name)
						self.locationSubject.onNext(lockDetails.lock.location)
						self.addressSubject.onNext(lockDetails.lock.address)
						self.isNotificationsOnSubject.onNext(lockDetails.lock.isActivityNotification)
						
						self.lockParticipantsSubject.onNext(lockDetails.participants)
						
					} else if let error = response.error {
						self.errorsSubject.onNext(error)
					}
				default: break;
				}
			}).disposed(by: disposeBag)
		
		
		loadLockHistorySubject.asObserver()
			.withLatestFrom(lockIDSubject.asObservable()) { (offset, lockID) in
				return (offset:offset, lockID: lockID)
			}.flatMap ({ values in
				service.getLockHistory(lockID: values.lockID, offset: values.offset).materialize()
			})
			.subscribe(onNext: { (event) in
				switch event {
				case .next(let response):
					if response.error == nil, let history = response.history {
						self.lockHistorySubject.onNext(history)
					} else  if let error = response.error {
						self.errorsSubject.onNext(error)
					}
				default: break
				}
			})
			.disposed(by: disposeBag)
		
		
	}
}
