//
//  LocksService.swift
//  Ausweis
//
//  Created by admin1 on 7/10/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

import RxSwift
import Ausweis_ioSDK

protocol LocksServiceProtocol {
	
	func getLockDetails(lockID: Int) -> Observable<(lock:LockDetailsResponse?, error: ResponseError?)>
	
	func getLockHistory(lockID: Int, offset: Int) -> Observable<(history:[LockHistory]?, error: ResponseError?)>
	
}

class LocksService: LocksServiceProtocol {
	
	
	
	

	private let sdk = ASWKeysLocksSDK()
	
	func getLockDetails(lockID: Int) -> Observable<(lock: LockDetailsResponse?, error: ResponseError?)> {
		return Observable.create { observer in
			self.sdk.getLockDetails(lockID, completion: { lockResponse in
				observer.onNext( (lock: lockResponse, error: nil))
			}, failure: { (error) in
				observer.onNext((lock: nil, error: error))
			})
			
			return Disposables.create()
		}
	}
	
	func getLockHistory(lockID: Int, offset:Int) -> Observable<(history: [LockHistory]?, error: ResponseError?)> {
		return Observable.create({ observer in
			self.sdk.getLockHistory(lockID, offset: offset, completion: { (history) in
				observer.onNext((history: history , error: nil))
			}, failure: { (error) in
				observer.onNext((history: nil, error: error))

			})
			
			return Disposables.create()
		})
	}
	
	
}
