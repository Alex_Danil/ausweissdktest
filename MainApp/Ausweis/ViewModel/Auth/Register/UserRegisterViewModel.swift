//
//  UserRegisterViewModel.swift
//  Ausweis
//
//  Created by admin1 on 6/22/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

class UserRegisterViewModel : ViewModelProtocol {
	
	struct Input {
		var firstName: AnyObserver<String>
		var lastName: AnyObserver<String>
		var password: AnyObserver<String>
		var email: AnyObserver<String>
		var phone: AnyObserver<String>
		var photoPath: AnyObserver<String?>
		var registerDidTap: AnyObserver<Void>
		var checkEmailDidTap: AnyObserver<Void>
	}
	
	struct Output {
		let registerResultObservable: Observable<Bool>
		let checkEmailResultObservable: Observable<Bool>
		let errorsObservable: Observable<ResponseError>
	}
	
	let input: Input
	let output: Output
	
	private let emailSubject = PublishSubject<String>()
	private let passwordSubject = PublishSubject<String>()
	private let firstNameSubject = PublishSubject<String>()
	private let lastNameSubject = PublishSubject<String>()
	private let photoSubject = PublishSubject<String?>()
	private let phoneSubject = PublishSubject<String>()
	
	private let registerDidTapSubject = PublishSubject<Void>()
	private let registerResultSubject = PublishSubject<Bool>()
	
	private let checkEmailDidTapSubject = PublishSubject<Void>()
	private let checkEmailResultSubject = PublishSubject<Bool>()
	
	private let errorsSubject = PublishSubject<ResponseError>()
	
	private var credentialsObservable: Observable<Credentials> {
		return Observable.combineLatest(emailSubject.asObservable(),
																		passwordSubject.asObservable(),
																		lastNameSubject.asObservable(),
																		firstNameSubject.asObservable(),
																		phoneSubject.asObservable(),
																		photoSubject.asObservable().startWith(nil)
		) { (email, password, lastName, firstName, phone, photo) in
			return Credentials(email: email, password: password, name: firstName + " " + lastName, phone: phone, imagePath: photo)
		}
	}
	
//	let registerService : RegisterServiceProtocol
	
	let disposeBag = DisposeBag()
	
	
	init<T:RegisterServiceProtocol>(_ registerService: T) {
		input = Input(firstName: firstNameSubject.asObserver(),
									lastName: lastNameSubject.asObserver(),
									password: passwordSubject.asObserver(),
									email: emailSubject.asObserver(),
									phone: phoneSubject.asObserver(),
									photoPath: photoSubject.asObserver(),
									registerDidTap: registerDidTapSubject.asObserver(),
									checkEmailDidTap: checkEmailDidTapSubject.asObserver())
		
		output = Output(registerResultObservable: registerResultSubject.asObserver(),
										checkEmailResultObservable: checkEmailResultSubject.asObserver(),
										errorsObservable: errorsSubject.asObserver())
	
		registerHandler(registerService)
		checkEmailHandler(registerService)
		
	}
	
	private func registerHandler(_ registerService: RegisterServiceProtocol) {
		registerDidTapSubject
			.withLatestFrom(credentialsObservable, resultSelector: { (funct, creds)  in
				return creds
			})
			.flatMapLatest { creds in
				return registerService.signUp(with: creds).materialize()
			}
			.single()
			.subscribe{ [weak self] event in
				switch event {
				case .next(let _event):
					if let response =  _event.element {
						
						if response.success {
							self?.registerResultSubject.onNext(response.success)
						} else if let error = response.error {
							self?.errorsSubject.onNext(error)
						}
						
					}
				default: break
				}
			}
			.disposed(by: disposeBag)
	}
	
	func checkEmailHandler(_ registerService: RegisterServiceProtocol) {
		checkEmailDidTapSubject.withLatestFrom(emailSubject.asObservable()).flatMapLatest { email in
			return registerService.checkEmailAvailable(with: email).materialize()
			}.subscribe(onNext:{ [weak self] (event) in
				switch event {
				case .next(let response):
					if response.success {
						self?.checkEmailResultSubject.onNext(response.success)
					} else if let error = response.error {
						self?.errorsSubject.onNext(error)
					}
				default: break
				}
			}).disposed(by: disposeBag)
	}
	
	func test () {

	}
	
}

