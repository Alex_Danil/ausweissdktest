//
//  RegisterService.swift
//  Ausweis
//
//  Created by admin1 on 6/23/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

struct Credentials {
	let email: String
	let password: String
	let name: String
	let phone: String
	let imagePath: String?
}

protocol RegisterServiceProtocol {
	func signUp(with credentials: Credentials) -> Observable<(success:Bool, error: ResponseError?)>
	
	func checkEmailAvailable(with email: String) -> Observable<(success:Bool, error: ResponseError?)>

}

class RegisterService: RegisterServiceProtocol {
	
	private let sdk = ASWUserSDK()
	
	func signUp(with credentials: Credentials) -> Observable<(success:Bool, error: ResponseError?)> {
		return Observable.create { observer in
			
			self.sdk.register(credentials.email, password: credentials.password, phoneNumber: credentials.phone, userName: credentials.name, photoPath: credentials.imagePath, completion: { (success) in
				observer.onNext((success: success , error: nil))
			}, failure: { (error) in				
				observer.onNext((success: false , error: error))
			})
			
			return Disposables.create()
		}
	}
	
	
	func checkEmailAvailable(with email: String) -> Observable<(success: Bool, error: ResponseError?)> {
		return Observable.create { observer in
			self.sdk.checkEmailAvailable(email, completion: { (success) in
				observer.onNext((success: success , error: nil))
			}, failure: { (error) in
				observer.onNext((success: false , error: error))
			})
			
			return Disposables.create()
			
		}
	}
}
