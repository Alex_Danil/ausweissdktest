//
//  UserViewModel.swift
//  Ausweis
//
//  Created by admin1 on 7/18/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

class UserViewModel : ViewModelProtocol {

	struct Input {
		var loadUser: AnyObserver<Void>
		var notifications: AnyObserver<Bool>
		var emailNotifications: AnyObserver<Bool>
		var password: AnyObserver<String>
		var email: AnyObserver<String>
		var photoPath: AnyObserver<String>
	}
	
	struct Output {
		let name: Observable<String>
		let email: Observable<String>
		let phone: Observable<String>
		let photo: Observable<URL>
		let isNotificationsEnabled: Observable<Bool>
		let isEmailNotificationsEnabled: Observable<Bool>
		
		let editSuccess: Observable<Void>
		
		let errors: Observable<ResponseError>
	}
	
	let input: Input
	let output: Output
	
	
	let isLoading = PublishSubject<Bool>()
	let logout = PublishSubject<Void>()

	
	//Get
	private let loadUserSubject = PublishSubject<Void>()
	
	private let emailSubject = ReplaySubject<String>.create(bufferSize: 1)
	private let phoneSubject = ReplaySubject<String>.create(bufferSize: 1)
	private let photoSubject = ReplaySubject<URL>.create(bufferSize: 1)
	
	private let fullNameSubject = ReplaySubject<String>.create(bufferSize: 1)
	
	private let isNotificationsEnabledSubject = ReplaySubject<Bool>.create(bufferSize: 1)
	private let isEmailNotificationsEnabledSubject = ReplaySubject<Bool>.create(bufferSize: 1)
	
	
	
	//Edit
	private let editEmailSubject = ReplaySubject<String>.create(bufferSize: 1)
	private let editPhoneSubject = ReplaySubject<String>.create(bufferSize: 1)
	private let editPhotoSubject = ReplaySubject<String>.create(bufferSize: 1)
	private let editPasswordSubject = ReplaySubject<String>.create(bufferSize: 1)

	private let editFullNameSubject = ReplaySubject<String>.create(bufferSize: 1)
	
	private let editNotificationsEnabledSubject = ReplaySubject<Bool>.create(bufferSize: 1)
	private let editEmailNotificationsEnabledSubject = ReplaySubject<Bool>.create(bufferSize: 1)
	
	private let editSuccessSubject = PublishSubject<Void>()
	
	//Errors
	private let errorsSubject = PublishSubject<ResponseError>()

	let disposeBag = DisposeBag()
	
	init<T: UserServiceProtocol>(service: T) {
		input = Input(loadUser: loadUserSubject.asObserver(),
									notifications: editNotificationsEnabledSubject.asObserver(),
									emailNotifications: editEmailNotificationsEnabledSubject.asObserver(),
									password: editPasswordSubject.asObserver(),
									email: editEmailSubject.asObserver(),
									photoPath: editPhotoSubject.asObserver())
		
		
		output = Output(name: fullNameSubject.asObserver(),
										email: emailSubject.asObserver(),
										phone: phoneSubject.asObserver(),
										photo: photoSubject.asObserver(),
										isNotificationsEnabled: isNotificationsEnabledSubject.asObserver(),
										isEmailNotificationsEnabled: isEmailNotificationsEnabledSubject.asObserver(),
										editSuccess: editSuccessSubject.asObserver(),
										errors: errorsSubject.asObserver())
		
		handleLogout(service)
		handleService(service)
		
	}
	
	private func handleLogout(_ service: UserServiceProtocol) {
		logout
			.subscribe(onNext: service.logout)
			.disposed(by: disposeBag)
	}
	
	private func handleService(_ service: UserServiceProtocol) {
		
		let user = service.loadUser()
		
		fullNameSubject.onNext(user?.name ?? "")
		emailSubject.onNext(user?.email ?? "")
		phoneSubject.onNext(user?.phone ?? "No phone added")
		
		isNotificationsEnabledSubject.onNext(user?.isNotificationsEnabled ?? false)
		isEmailNotificationsEnabledSubject.onNext(user?.emailNotificationsEnabled ?? false)
		
		if let photoURL = URL(string: user?.photo ?? "") {
				photoSubject.onNext(photoURL)
		}
		
		subscribeEditService(service, userID: user?.identifier ?? 0)
		
	}
	
	private func subscribeEditService(_ service: UserServiceProtocol, userID: Int) {
		
		editNotificationsEnabledSubject
			.flatMapLatest { enabled  -> Observable<Event<(success: Bool, error: ResponseError?)>> in
				self.isLoading.onNext(true)
				return service.changeSettings(.notifications, value: enabled, userID: userID).materialize()
			}
			.subscribe(onNext: observeEditResponse)
			.disposed(by: disposeBag)
		
		editEmailNotificationsEnabledSubject
			.flatMapLatest { enabled  -> Observable<Event<(success: Bool, error: ResponseError?)>> in
				self.isLoading.onNext(true)
				return service.changeSettings(.emailNotifications, value: enabled, userID: userID).materialize()
			}
			.subscribe(onNext: observeEditResponse)
			.disposed(by: disposeBag)
		
		editPhotoSubject.flatMapLatest { photoPath  -> Observable<Event<(success: Bool, error: ResponseError?)>> in
			self.isLoading.onNext(true)
				return service.changePhoto(photoPath, userID: userID).materialize()
			}
			.subscribe(onNext: observeEditResponse)
			.disposed(by: disposeBag)
	}
	
	private func observeEditResponse(_ event: Event<(success:Bool, error: ResponseError?)>) {
		
		isLoading.onNext(false)
		
		switch event {
		case .next(let response):
			if response.success {
				self.editSuccessSubject.onNext(())
			} else if let error = response.error {
				self.errorsSubject.onNext(error)
			}
		default: break
		}
		
	}
	
	
}
