//
//  UserService.swift
//  Ausweis
//
//  Created by admin1 on 7/18/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

protocol UserServiceProtocol {
	func loadUser() -> User!
	
	func logout()
	
	func changeSettings(_ field: UserField, value: Any, userID: Int) -> Observable<(success:Bool, error: ResponseError?)>
	
	func changePhoto(_ photoPath: String , userID: Int) -> Observable<(success: Bool, error: ResponseError?)>
	
}


class UserService: UserServiceProtocol {
	
	private let sdk = ASWUserSDK()

	
	func changeSettings(_ field: UserField, value: Any, userID: Int) -> Observable<(success: Bool, error: ResponseError?)> {
		return Observable.create { observer in
			
			self.sdk.changeUserSettings(field, value: value, userID: userID, completion: { success in
				observer.onNext((success: success , error: nil))
			}, failure: { error in
				observer.onNext((success: false , error: error))
			})
			
			return Disposables.create()
		} 
	}
	
	func changePhoto(_ photoPath: String , userID: Int) -> Observable<(success: Bool, error: ResponseError?)> {
		return Observable.create { observer in
			
			self.sdk.changeUserPhoto(photoPath, userID: userID, completion: { success in
				observer.onNext((success: success , error: nil))
			}, failure: { error in
				observer.onNext((success: false , error: error))
			})
			
			return Disposables.create()
		}
	}

	private weak var realmManager = RealmManager.shared
	
	func loadUser() -> User! {
		 return realmManager!.fetch(with:User.all).first
	}
	
	func logout() {
		sdk.logout()
	}
}

