//
//  KeysService.swift
//  Ausweis
//
//  Created by admin1 on 7/2/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

protocol KeysServiceProtocol {
	
	func getAllKeys() -> Observable<(keys:[Key]?, error: ResponseError?)>
	
	func openKey(_ key: Key, action: LockAction) -> Observable<(success:Bool, error: ResponseError?)>
	
	func shareKey(_ key: KeyShare) -> Observable<(key: Key?, error: ResponseError?)>
	
}

class KeysService: KeysServiceProtocol {
	
	private let sdk = ASWKeysLocksSDK()

	func openKey(_ key: Key, action: LockAction) -> Observable<(success: Bool, error: ResponseError?)> {
		return Observable.create({ observer in
			self.sdk.openLock(withKey:key, action: action, completion: { success in
				observer.onNext((success: success, error: nil))
			}, failure: { (error) in
				observer.onNext((success: false, error: error))
			})
			
			return Disposables.create()
		})
	}
	
	
	func shareKey(_ key: KeyShare) -> Observable<(key: Key?, error: ResponseError?)> {
		return Observable.create { observer in
	
			self.sdk.shareLock(key, completion: { (key) in
				observer.onNext((key: key, error: nil))
			}, failure: { (error) in
				observer.onNext((key: nil, error: error))
			})
						
			return Disposables.create()
		}
	}
	
	
	func getAllKeys() -> Observable<(keys: [Key]?, error: ResponseError?)> {
		return Observable.create { observer in
			self.sdk.getAllKeys({ keys in
				observer.onNext((keys: keys, error: nil))
			}, failure: { error in
				observer.onNext((keys: nil, error: error))
			})
			
			return Disposables.create()
		}
	}
	
}
