//
//  ShareKeyViewModel.swift
//  Ausweis
//
//  Created by admin1 on 7/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

class ShareKeyViewModel: ViewModelProtocol {
	
	enum ShareType {
		case email,
		link,
		webApp
	}
	
	struct Input {
		var shareType: BehaviorSubject<ShareType>
		var emailString: AnyObserver<String?>
		var isEmail : AnyObserver<Bool>
		var isLinkInvite : AnyObserver<Bool>
		var isLimited: AnyObserver<Bool>
		var limitStart: AnyObserver<Date>
		var limitEnd: AnyObserver<Date>
		var isOneTimeKey: AnyObserver<Bool>
		var isActivityAccess: AnyObserver<Bool>
		
		var schedule: AnyObserver<KeySchedule>
		var saveSchedule: AnyObserver<Void>
	}
	
	struct Output {
		var limitStartString: Observable<String>
		var limitEndString: Observable<String>
		var keyShareLink: Observable<String>
		
		var schedules: Observable<[KeySchedule]>
	}
	
	let input: Input
	let output: Output
	
	let isLoading = PublishSubject<Bool>()

	
	// Input
	
	private let lockID: BehaviorSubject<Int>
	
	private let emailStringSubject = PublishSubject<String?>()
	
	private let isLimitedSubject = PublishSubject<Bool>()
	private let isEmailSubject = PublishSubject<Bool>()
	private let isLinkSubject = PublishSubject<Bool>()
	private let limitStartSubject = PublishSubject<Date>()
	private let limitEndSubject = PublishSubject<Date>()
	
	private let isOneTimeKeySubject = PublishSubject<Bool>()
	private let isActivityAccessSubject = PublishSubject<Bool>()
	
	private let scheduleSubject = PublishSubject<KeySchedule>()
	
	private let saveSubject = PublishSubject<Void>()
	
	private let errorsSubject = PublishSubject<ResponseError>()
	
	// Output
	
	private let limitStartStringSubject = PublishSubject<String>()
	private let limitEndStringSubject = PublishSubject<String>()
	private let keyShareLinkSubject = ReplaySubject<String>.create(bufferSize: 1)
	private let schedulesSubject = ReplaySubject<[KeySchedule]>.create(bufferSize: 1)
	
	let disposeBag = DisposeBag()
	
	private var shareObservable: Observable<KeyShare> {
		return Observable.combineLatest(lockID.asObserver(),
																		isLimitedSubject.asObserver(),
																		limitStartSubject.asObserver(),
																		limitEndSubject.asObserver(),
																		isOneTimeKeySubject.asObserver() ,
																		isActivityAccessSubject.asObserver(),
																		input.shareType.asObserver(),
																		emailStringSubject.asObserver().startWith(nil)
																		)
		{ (lockID, isLimited , limitStart , limitEnd , isOneTimeKey, isActivityAccess, shareType, email) -> KeyShare in
			let keyShare = KeyShare(forLock: lockID, isLimited: isLimited, limitStart: limitStart, limitEnd: limitEnd, isOneTimeKey: !isOneTimeKey, isActivityAccess: isActivityAccess, schedule: nil)
			keyShare.isLinkInvite = shareType == .link
			keyShare.email = email
			return keyShare
		}
	}
	
	init<T:KeysServiceProtocol>(keysService service: T, lockID: Int, shareType: ShareType) {
		self.lockID = BehaviorSubject(value: lockID)
		
		input = Input(shareType: BehaviorSubject<ShareType>(value: shareType),
									emailString: emailStringSubject.asObserver(),
									isEmail: isEmailSubject.asObserver(),
									isLinkInvite: isLinkSubject.asObserver(),
									isLimited: isLimitedSubject.asObserver(),
									limitStart: limitStartSubject.asObserver(),
									limitEnd: limitEndSubject.asObserver(),
									isOneTimeKey: isOneTimeKeySubject.asObserver(),
									isActivityAccess: isActivityAccessSubject.asObserver(),
									schedule: scheduleSubject.asObserver(),
									saveSchedule: saveSubject.asObserver())
		
		output = Output(limitStartString: limitStartStringSubject.asObserver(),
										limitEndString: limitEndStringSubject.asObserver(),
										keyShareLink: keyShareLinkSubject.asObserver(),
										schedules: schedulesSubject.asObserver())
		
		handleService(service)
		schedulesSubject.onNext([])
		handleIO()
	}
	
	private func handleService(_ service: KeysServiceProtocol) {

		saveSubject.asObservable()
			.withLatestFrom(shareObservable)
			.withLatestFrom(schedulesSubject.startWith([]), resultSelector: { (keyShare, schedules) -> KeyShare in
				keyShare.addSchedules(schedules)
				return keyShare
			})
			.flatMapLatest { (keyShare) ->  Observable<Event<(key: Key?, error: ResponseError?)>> in
				self.isLoading.onNext(true)
				return service.shareKey(keyShare).materialize()
			}.subscribe { [unowned self] event in
				switch event {
				case .next(let _event):
					if let response =  _event.element {
						self.isLoading.onNext(false)

						if response.error == nil, let key = response.key {
							self.keyShareLinkSubject.onNext(key.shareUrl ?? "")
						} else if let error = response.error {
							self.errorsSubject.onNext(error)
						}
					}
				default: break
				}
			}
			.disposed(by: disposeBag)
		
	}
	
	func handleIO() {
		
		let formatter = DateFormatter()
		formatter.dateFormat = "d MMM yyyy HH:mm"
		limitStartSubject.asObservable().subscribe(onNext: { date in
			self.limitStartStringSubject.onNext(formatter.string(from: date))
		}).disposed(by: disposeBag)
		
		limitEndSubject.asObservable().subscribe(onNext: { date in
			self.limitEndStringSubject.onNext(formatter.string(from: date))
		}).disposed(by: disposeBag)
		
		scheduleSubject.asObservable()
			.withLatestFrom(schedulesSubject.asObservable(), resultSelector: { (schedule, schedules) in
				var _schedules = schedules
				_schedules.append(schedule)
				return _schedules
			})
			.subscribe(onNext: { (schedule) in
				self.schedulesSubject.onNext(schedule)
			}).disposed(by: disposeBag)
		
	}
	
}
