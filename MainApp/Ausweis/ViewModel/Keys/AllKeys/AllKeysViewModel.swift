//
//  AllKeysViewModel.swift
//  Ausweis
//
//  Created by admin1 on 7/2/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK


class AllKeysViewModel: ViewModelProtocol {
	
	struct Input {
		var loadAllKeys: AnyObserver<Void>
		var searchQuery: AnyObserver<String>
	}
	
	struct Output {
		let allKeysResult: Observable<[Key]?>
		let ownerKeysResult: Observable<[Key]>
		let sharedKeysResult: Observable<[Key]>
		
		let filteredKeysResult: Observable<[Key]?>
		let errorsObservable: Observable<ResponseError>
	}
	
	let input: Input
	let output: Output
	
	private let allKeysSubject = ReplaySubject<[Key]?>.create(bufferSize: 1)
	private let filteredSubject = PublishSubject<[Key]?>()
	private let ownerKeysSubject = PublishSubject<[Key]>()
	private let sharedKeysSubject = PublishSubject<[Key]>()

	private let loadAllKeysSubject = PublishSubject<Void>()
	private let searchQuerySubject = PublishSubject<String>()

	private let errorsSubject = PublishSubject<ResponseError>()
	
	let disposeBag = DisposeBag()

	init<T:KeysServiceProtocol>(keysService service: T) {
		input = Input(loadAllKeys: loadAllKeysSubject.asObserver(),
									searchQuery: searchQuerySubject.asObserver())
		
		output = Output(allKeysResult: allKeysSubject.asObserver(),
										ownerKeysResult: ownerKeysSubject.asObserver(),
										sharedKeysResult: sharedKeysSubject.asObserver(),
										filteredKeysResult: filteredSubject.asObserver(),
										errorsObservable: errorsSubject.asObserver())
		
		handleService(service)
		filterHandler()
	}
	
	private func handleService(_ service: KeysServiceProtocol) {
        
    
		loadAllKeysSubject.flatMap {
			return service.getAllKeys().materialize()
		}
			.subscribe { [unowned self] event in
				switch event {
				case .next(let _event):
					if let response =  _event.element {
						
						if response.error == nil {
							self.allKeysSubject.onNext(response.keys)
							
							self.sharedKeysSubject.onNext(response.keys!.compactMap { $0 }.filter { $0.lockInfo.accessType == .guest })
							self.ownerKeysSubject.onNext(response.keys!.compactMap { $0 }.filter { $0.lockInfo.accessType == .owner || $0.lockInfo.accessType == .admin })
						} else if let error = response.error {
							self.errorsSubject.onNext(error)
						}
						
					}
				default: break
				}
		}.disposed(by: disposeBag)
	}
	
	private func filterHandler() {
		searchQuerySubject.subscribe(onNext: { query in
			if query.isEmpty {
				//				self.sharedKeysSubject.onNext(
				self.allKeysSubject.subscribe(onNext: { (keys) in
					self.sharedKeysSubject.onNext(keys!.compactMap { $0 }.filter { $0.lockInfo.accessType == .guest })
					self.ownerKeysSubject.onNext(keys!.compactMap { $0 }.filter { $0.lockInfo.accessType == .owner})
				}).disposed(by: self.disposeBag)
			} else {
				self.allKeysSubject.asObservable().subscribe(onNext: { (keys) in
					self.sharedKeysSubject.onNext(keys!.compactMap { $0 }
						.filter { $0.lockInfo.accessType == .guest }
						.filter { $0.lockInfo.name.lowercased().contains(query.lowercased())}
					)
					self.ownerKeysSubject.onNext(keys!.compactMap { $0 }
						.filter { $0.lockInfo.accessType == .owner}
						.filter { $0.lockInfo.name.lowercased().contains(query.lowercased())}
					)
				}).disposed(by: self.disposeBag)
			}
		}).disposed(by: disposeBag)
	}
	
	
}
