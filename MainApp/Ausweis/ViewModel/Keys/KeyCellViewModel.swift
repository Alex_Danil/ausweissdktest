//
//  KeyCellViewModel.swift
//  Ausweis
//
//  Created by admin1 on 7/3/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

class KeyCellViewModel: ViewModelProtocol {
	
	struct Input {
		fileprivate let key : Key
		var openKey: AnyObserver<LockAction>
	}
	
	struct Output {
		let name: Variable<String>
		let alarm: Variable<Bool>
		let openResponse: Observable<Bool>
		let errorsObservable: Observable<ResponseError>
		let isOpenClose: Variable<Bool>
	}
	
	private let openResponseSubject = PublishSubject<Bool>()
	private let openCloseSubject = PublishSubject<Bool>()
	private let openKeySubject = PublishSubject<LockAction>()
	
	private let errorsSubject = PublishSubject<ResponseError>()

	let disposeBag = DisposeBag()
	
	let input: Input
	let output: Output
	
	private let keyService: KeysServiceProtocol
	
	init(key: Key) {
		input = Input(key: key,
									openKey: openKeySubject.asObserver())

		output = Output(name: Variable(key.lockInfo.name),
										alarm: Variable(key.lockInfo.isSecurityMode ?? false),
										openResponse: openResponseSubject.asObserver(),
										errorsObservable: errorsSubject.asObserver(),
										isOpenClose: Variable(key.lockInfo.isDoubleRelay || key.lockInfo.isAlwaysOpened ))
		
		keyService = KeysService()
		
		handleOpenKey(withService: keyService)
	}
	
	private func handleOpenKey(withService service: KeysServiceProtocol) {
		
		openKeySubject
			.flatMapLatest { [unowned self] action in
				return service.openKey(self.input.key, action: action).materialize()
			}
			.subscribe(onNext: { (event) in
				switch event {
				case .next(let response) :
					if response.error == nil {
						self.openResponseSubject.onNext(response.success)
					} else if let error = response.error {
						self.errorsSubject.onNext(error)
					}
				default:
					break
				}
			
			})
			.disposed(by: disposeBag)
		
	}
	
}
