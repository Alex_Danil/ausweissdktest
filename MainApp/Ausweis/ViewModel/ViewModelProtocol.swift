//
//  ViewModelProtocol.swift
//  Ausweis
//
//  Created by admin1 on 6/22/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation

protocol ViewModelProtocol: class {
	associatedtype Input
	associatedtype Output
}
