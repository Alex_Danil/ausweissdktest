//
//  NotificationsService.swift
//  Ausweis
//
//  Created by admin1 on 7/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import Ausweis_ioSDK
import RxSwift

protocol NotificationsServiceProtocol {
	
	func getNotifications(_ offset: Int) -> Observable<(notifications: [LockNotification]?, error: ResponseError?)>

	
}


class NotificationsService : NotificationsServiceProtocol {
	
	let sdk = ASWUserSDK()
	
	func getNotifications(_ offset: Int) -> Observable<(notifications: [LockNotification]?, error: ResponseError?)> {
		return Observable.create({ observer in
			
			self.sdk.getNotifications(offset, completion: { (notifications) in
				observer.onNext((notifications: notifications, error: nil))
			}, failure: { (error) in
				observer.onNext((notifications: nil, error: error))
			})
			
			return Disposables.create()
			
		})
	}
	
}
