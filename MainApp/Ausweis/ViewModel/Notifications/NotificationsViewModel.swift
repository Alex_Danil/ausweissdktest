//
//  NotificationsViewModel.swift
//  Ausweis
//
//  Created by admin1 on 7/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift
import Ausweis_ioSDK

class NotificationsViewModel: ViewModelProtocol {
	
	struct Input {
		let loadNotifications: AnyObserver<Int>
	}
	
	struct Output {
		let notifications: Observable<[LockNotification]>
		let notificationsSource: Observable<[NotificationsSection]>
	}
	
	
	let input : Input
	let output : Output
	
	private let notificationsSubject = ReplaySubject<[LockNotification]>.createUnbounded()
	private let loadNotificationsSubject = ReplaySubject<Int>.create(bufferSize: 1)
	private let notificationsSourceSubject = ReplaySubject<[NotificationsSection]>.create(bufferSize: 1)
	
	private var lastLoadedIndex = 0
	
	let disposeBag = DisposeBag()
	
	init<T:NotificationsServiceProtocol>(service: T) {
		
		input = Input(loadNotifications: loadNotificationsSubject.asObserver())
		output = Output(notifications: notificationsSubject.asObserver(),
										notificationsSource: notificationsSourceSubject)
		
		handleService(service)
		
		var lockNotifications =  [LockNotification]()

		notificationsSubject.subscribe(onNext: { (partLockNotifications) in
			lockNotifications.append(contentsOf: partLockNotifications)
			self.sortNotifications(lockNotifications)
		}).disposed(by: disposeBag)
	}
	
	func handleService(_ service: NotificationsServiceProtocol){
		
		loadNotificationsSubject
			.flatMap { (offset) -> Observable<Event<(notifications: [LockNotification]?, error: ResponseError?)>> in
				self.lastLoadedIndex = self.lastLoadedIndex + offset
				return service.getNotifications(self.lastLoadedIndex).materialize()
			}
			.subscribe(onNext: { (event) in
				switch event {
				case .next(let response):
					if response.error == nil, let notifications = response.notifications {
						self.notificationsSubject.onNext(notifications)
//						self.sortNotifications( notifications)
					} else {
						// TODO: Error handle
						print(response.error)
					}
				default: break
				}
			})
			.disposed(by: disposeBag)
	}
	
	func sortNotifications(_ notifications: [LockNotification] ) {
		
		var notificationSource = [String: [LockNotification]]()
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "d MMM yyyy"

		
		notifications.forEach { (notification) in
			if let date = Date.aswDateFormatter( notification.created!) {
				let dateKey = dateFormatter.string(from: date)
				if notificationSource[dateKey] != nil {
					notificationSource[dateKey]?.append(notification)
				} else {
					notificationSource[dateKey] = [notification]
				}
			}
		}
		
		let dataSource = notificationSource
			.map { source  in
				return NotificationsSection(title: source.key , items: source.value)
			}
			.sorted { (firstSection, secondSection) -> Bool in
				return dateFormatter.date(from: firstSection.header)! > dateFormatter.date(from: secondSection.header)!
			}
		
		notificationsSourceSubject.onNext(dataSource)
		
	}
	
	
}
