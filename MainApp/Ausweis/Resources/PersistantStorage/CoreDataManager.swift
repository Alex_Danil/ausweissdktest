//
//  RealmManager.swift
//  Ausweis
//
//  Created by admin1 on 6/18/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RealmSwift

struct FetchRequest<Model, RealmObject: Object> {
	let predicate: NSPredicate?
	let sortDescriptors: [SortDescriptor]
	let transformer: (Results<RealmObject>) -> Model
}

class RealmManager {
	
	static let shared = RealmManager()
	
	lazy var realm: Realm! = {
		do {
			return try Realm()
		} catch let error as NSError {
			// handle error
			//TODO: show allert error, about
			print("realm error: \(error)")
			return nil
		} catch {
			print("realm error")
			let error = NSError(domain: "ausweis.realm_init", code: 0, userInfo: [:])
			return nil
		}
	}()
	
	func deleteAll() {
		do {
			try realm?.write {
				realm?.deleteAll()
			}
		} catch {
			print("realm error")
		}
	}
	
	func migrate() {
		
		let currentVersion: UInt64 = 1
		
		let config = Realm.Configuration(
			// Set the new schema version. This must be greater than the previously used
			// version (if you've never set a schema version before, the version is 0).
			schemaVersion: currentVersion,
			
			// Set the block which will be called automatically when opening a Realm with
			// a schema version lower than the one set above
			migrationBlock: { migration, oldSchemaVersion in
				
		})
		Realm.Configuration.defaultConfiguration = config
	}
	
	func createOrUpdate<Model, RealmObject: Object>(model: Model, with reverseTransformer: (Model) -> RealmObject) {
		let object = reverseTransformer(model)
		try! realm.write {
			realm.add(object, update: true)
		}
	}
	
	func fetch<Model, RealmObject: Object>(with request: FetchRequest<Model,RealmObject>) -> Model {
		var results = realm.objects(RealmObject.self)
		if let predicate = request.predicate {
			results = results.filter(predicate)
		}
		if request.sortDescriptors.count > 0 {
			results = results.sorted(by: request.sortDescriptors)
		}
		return request.transformer(results)
	}
	
	func delete<RealmObject: Object>(type: RealmObject.Type, with primaryKey: String) {
		let object = realm.object(ofType: type, forPrimaryKey: primaryKey)
		if let object = object {
			try! realm.write {
				realm.delete(object)
			}
		}
	}
	
}




