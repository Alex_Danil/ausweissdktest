//
//  Validator.swift
//  Ausweis
//
//  Created by admin1 on 6/18/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import Validator

class ValidationManager {
	
	static func validateEmail(_ email: String?) -> (success: Bool, errorString: String?) {
		
		guard let email = email else {
			return (false,"EMAIL_EMPTY".localized)
		}
		
		let rule = ValidationRulePattern(pattern: EmailValidationPattern.standard, error: ValidationError(message: "INVALID_EMAIL".localized))

		let result = email.validate(rule: rule)
		
		switch result {
		case .valid: return (true, nil)
		case .invalid(let failures):
			let messages : [String] = failures.flatMap { ($0 as? ValidationError)?.message }
			return (false, messages.first ?? "")
		}
		
	}
	
	
}
