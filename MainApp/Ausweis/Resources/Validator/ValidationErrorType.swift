//
//  ValidationErrorType.swift
//  Ausweis
//
//  Created by admin1 on 6/18/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation



struct ValidationError: Error {
	
	public let message: String
	
	public init(message m: String) {
		message = m
	}
}
