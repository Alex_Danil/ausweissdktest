//
//  UserModel.swift
//  Ausweis
//
//  Created by admin1 on 6/18/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RealmSwift
import Ausweis_ioSDK

final class UserObject: Object {
	
	@objc dynamic var name: String = ""
	@objc dynamic var photo: String!
//	@objc dynamic var notificationSettings: [String:Bool]!
	@objc dynamic var email: String = ""
	@objc dynamic var phone: String!
	@objc dynamic var identifier: Int = 0
	
	override open class func primaryKey() -> String? { return "identifier" }
	
	convenience init(user: User) {
		self.init()
		
		self.name = user.name
		self.email = user.email
//		self.notificationSettings = user.notificationSettings
		self.photo = user.photo
		self.phone = user.phone
		self.identifier = user.identifier!
	}
	
}


extension User {
	
	static let all = FetchRequest<[User], UserObject>(predicate: nil, sortDescriptors: [], transformer: { $0.map(User.init) })
	
	convenience init(object: UserObject) {
		self.init()
		// Assign properties here
		self.name = object.name
		self.email = object.email
//		self.notificationSettings = nil
		self.photo = object.photo
		self.phone = object.phone
		self.identifier = object.identifier
		
	}
}
