//
//  LockHistoryModel.swift
//  Ausweis
//
//  Created by admin1 on 7/19/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import Ausweis_ioSDK

extension LockHistory {
	
	func historyCellViewModel() -> (date: String, photoURL: String?, name: String?) {
		let name = self.user.name
		let date = self.dateString
		let photo = self.user.photo
		
		return (date,photo,name)
	}
	
}
