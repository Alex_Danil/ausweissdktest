//
//  NotificationsModel.swift
//  Ausweis
//
//  Created by admin1 on 7/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import Ausweis_ioSDK

extension LockNotification {
	
	func cellViewModel() -> (date: String?, photoURL: String?, text: String?, title: String?) {
		
		var dateString = ""
		if let date = Date.aswDateFormatter(self.created!) {
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = "hh:mm"
			dateString = dateFormatter.string(from: date)
		}
		
		let title = self.subject
		let photo = self.userPhoto
		let message = self.message
		
		return (dateString,photo,message,title)
	}
	
	
}
