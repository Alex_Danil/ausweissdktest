//
// LoginRouter.swift
// Ausweis.ioSDKTest
//
// Created by admin1 on 4/24/18.
//Copyright © 2018 TIW. All rights reserved.
//

import UIKit

struct LoginRouter: Router {
	
	enum Route: String {
		case onboarding = "OnboardingViewController"
		case login = "LoginScreenViewController"
		case signUp = "RegistrationPageViewController"
		case forgotPassword = "ForgotPasswordController"
		case digitConfirmation = "ConfirmationController"
		
	}
	
	
	init() {}
	init<T>(viewModel: T) {}
	
	func route(to routeID: LoginRouter.Route, from context: UIViewController?, parameters: Any?) {
				
		var vc: UIViewController!
		
		switch routeID {
		case .onboarding:
			vc = OnboardingViewController(nibName: routeID.rawValue, bundle: nil)
		case .login :
			vc = LoginScreenViewController(nibName: routeID.rawValue, bundle: nil)
		case .signUp :
			vc = RegistrationPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
		default:
			return
		}
		
		guard let contextVC = context, contextVC.navigationController != nil else {
			let navigationVC = MainNavigationController(rootViewController: vc)
			
			guard let window = UIApplication.shared.delegate?.window! else {
				fatalError("No viewController initialized in \(self)")
			}
			
			window.rootViewController = navigationVC
			return
		}
		
		contextVC.navigationController?.pushViewController(vc, animated: true)

	}
	
}


