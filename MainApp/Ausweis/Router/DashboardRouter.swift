//
// DashboardRouter.swift
// Ausweis.ioSDKTest
//
// Created by admin1 on 4/26/18.
//Copyright © 2018 TIW. All rights reserved.
//

import UIKit
import Ausweis_ioSDK

struct DashboardRouter : Router {
	
	enum Route : String {
//		case dashboard = "DashboardViewController"
		case mainTabBar = "MainTabBarViewController"
		case detail = "LockDetailsViewController"
		case shareAccess = "ShareAccessTableViewController"
		case schedule = "ScheduleTableViewController"
		case history = "HistoryTableViewController"
		case shareEmail = "EmailShareViewController"
		case shareLink = "SharedLinkViewController"
	}

	init() {}
	
	init<T>(viewModel: T) {}
	
	func route(to routeID: DashboardRouter.Route, from context: UIViewController?, parameters: Any?) {
		var vc: UIViewController!
		
		switch routeID {
		case .mainTabBar:
			let storyboard = UIStoryboard(name: "MainTabBar", bundle: nil)
			vc = storyboard.instantiateInitialViewController()
			
		case .detail:
			guard let lock = parameters as? LockResponse else {
				return
			}
			vc = LockDetailsViewController(nibName: routeID.rawValue, bundle: nil, lock: lock)
			
		case .shareAccess:
			let _vc = UIStoryboard(name: "ShareAccess", bundle: nil).instantiateViewController(withIdentifier: routeID.rawValue) as! ShareAccessTableViewController
			
			guard let shareParams = parameters as? [String: Any], let lockID = shareParams["lockID"] as? Int, let shareType = shareParams["shareType"] as? ShareKeyViewModel.ShareType else {
				return
			}
			
			_vc.viewModel = ShareKeyViewModel(keysService: KeysService(), lockID: lockID, shareType: shareType)
			vc = _vc
			
		case .schedule:
			let _vc = UIStoryboard(name: "ShareAccess", bundle: nil).instantiateViewController(withIdentifier: routeID.rawValue) as! ScheduleTableViewController
			_vc.viewModel = parameters as! ShareKeyViewModel
			
			vc = _vc
			
		case .history :
			vc = HistoryTableViewController(nibName: routeID.rawValue, bundle: nil, viewModel: parameters as! LockViewModel)
		case .shareEmail :
			
			guard let vm = parameters as? ShareKeyViewModel else {
				return
			}
			
			vc = (UIStoryboard.init(name: "ShareAccess", bundle: nil).instantiateViewController(withIdentifier: routeID.rawValue) as! EmailShareViewController).addViewModel(vm)
		case .shareLink:
			guard let vm = parameters as? ShareKeyViewModel else {
				return
			}
			
			vc = (UIStoryboard.init(name: "ShareAccess", bundle: nil).instantiateViewController(withIdentifier: routeID.rawValue) as! SharedLinkViewController).addViewModel(vm)
		}
		
		DispatchQueue.main.async {
			guard let contextVC = context, contextVC.navigationController != nil, routeID != .mainTabBar else {
				let navigationVC = vc
				
				guard let window = UIApplication.shared.delegate?.window! else {
					fatalError("No viewController initialized in \(self)")
				}
				
				window.rootViewController = navigationVC
				return
			}
			
			contextVC.navigationController?.pushViewController(vc, animated: true)
		}
	}
	
}


