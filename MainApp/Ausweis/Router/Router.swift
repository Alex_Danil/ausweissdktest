//
//  Router.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 4/23/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

protocol Router {
	
	associatedtype Route
	
	func route(to routeID: Route, from context: UIViewController?, parameters: Any?)
	
	init()
	init<T>(viewModel: T)
}

extension Router {
	
	init<T>(viewModel: T) {
		self.init()
	}
	
	static func showAlert(message m: String? , fromVC: UIViewController?) {
		guard let fromVC = fromVC else { return }
		DispatchQueue.main.async {
			let vc = UIAlertController.init(title: "Error", message: m ?? "UNDEFINED_ERROR".localized, preferredStyle: .alert)
			
			let action = UIAlertAction(title: "OK", style: .default , handler: { (action) in
				vc.dismiss(animated: true, completion: nil)
			})
			vc.addAction(action)
			
			fromVC.present(vc, animated: true, completion: nil)
		}
	}
	
	static func logout() {
		
		let vc = LoginScreenViewController(nibName: "LoginScreenViewController", bundle: nil)
		UIApplication.shared.keyWindow?.rootViewController = vc

	}
	
	static var loadVC : LoadingViewController {
		return LoadingViewController(nibName: "LoadingViewController", bundle: nil)
	}
	
}
