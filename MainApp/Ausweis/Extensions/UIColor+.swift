//
//  UIColor+.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 6/15/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit


extension UIColor {
	func colorFromHexString (_ hex:String) -> UIColor {
		var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
		
		if (cString.hasPrefix("#")) {
			cString.remove(at: cString.startIndex)
		}
		
		if ((cString.characters.count) != 6) {
			return UIColor.gray
		}
		
		var rgbValue:UInt32 = 0
		Scanner(string: cString).scanHexInt32(&rgbValue)
		
		return UIColor(
			red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
			green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
			blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
			alpha: CGFloat(1.0)
		)
	}
}


extension UIColor {
	
	static var mainWhite: UIColor {
		return UIColor(white: 242.0 / 255.0, alpha: 1.0)
	}
	
	static var blackTwo: UIColor {
		return UIColor(red: 9.0 / 255.0, green: 9.0 / 255.0, blue: 9.0 / 255.0, alpha: 1)
	}
	
	static var blackThree: UIColor {
		return UIColor(white: 16.0 / 255.0, alpha: 1.0)
	}
	
	static var turquoiseGreen: UIColor {
		return UIColor(red: 0.0, green: 230.0 / 255.0, blue: 118.0 / 255.0, alpha: 1.0)
	}
	
	static var whiteTwo: UIColor {
		return UIColor(white: 1.0, alpha: 1.0)
	}
	
	static var greenishTurquoise0: UIColor {
		return UIColor(red: 0.0, green: 244.0 / 255.0, blue: 174.0 / 255.0, alpha: 0.0)
	}
	
	static var turquoiseGreen2: UIColor {
		return UIColor(red: 0.0, green: 230.0 / 255.0, blue: 118.0 / 255.0, alpha: 1)
	}
	
	static var mainBlack: UIColor {
		return UIColor(white: 0.0, alpha: 1.0)
	}
	
	static var titleBlack: UIColor {
		return UIColor(red: 66.0 / 255.0, green: 66.0 / 255.0, blue: 66.0 / 255.0, alpha: 1)
	}
	
	static var titleWhite: UIColor {
		return UIColor(red: 232.0 / 255.0, green: 232.0 / 255.0, blue: 232.0 / 255.0, alpha: 1)
	}
	
	static var textBlack: UIColor {
		return UIColor(red: 106.0 / 255.0, green: 106.0 / 255.0, blue: 106.0 / 255.0, alpha: 1)
	}
	
	static var textWhite: UIColor {
		return UIColor(red: 196.0 / 255.0, green: 196.0 / 255.0, blue: 196.0 / 255.0, alpha: 1)
	}
	
	static var headerBlack: UIColor {
		return UIColor(red: 140.0 / 255.0, green: 140.0 / 255.0, blue: 140.0 / 255.0, alpha: 1)
	}
	
	static var headerWhite: UIColor {
		return UIColor(red: 140.0 / 255.0, green: 140.0 / 255.0, blue: 140.0 / 255.0, alpha: 1)
	}
	
	static var cellBlack: UIColor {
		return UIColor(red: 30.0 / 255.0, green: 30.0 / 255.0, blue: 30.0 / 255.0, alpha: 1)
	}
	
	static var cellWhite: UIColor {
		return UIColor(white: 1.0, alpha: 1.0)
	}
	
	
}
