//
//  CALayer+.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 6/15/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

extension CALayer {
	
	func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
		
		let border = CALayer()
		
		switch edge {
		case .top:
			border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
		case .bottom:
			border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
		case .left:
			border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
		case .right:
			border.frame = CGRect(x: self.frame.width - thickness, y: 0, width: thickness, height: self.frame.height)
		default:
			return
		}
		
		border.backgroundColor = color.cgColor;
		
		self.addSublayer(border)
	}
	
}
