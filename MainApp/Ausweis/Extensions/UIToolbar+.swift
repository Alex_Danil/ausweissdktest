//
//  UIToolbar+.swift
//  Ausweis
//
//  Created by admin1 on 7/13/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import UIKit

extension UIToolbar {
	
	func ToolbarPiker(buttonItem : UIBarButtonItem) -> UIToolbar {
	
		let toolBar = UIToolbar()
		
		toolBar.barStyle = UIBarStyle.default
		toolBar.isTranslucent = true
		toolBar.tintColor = UIColor.black
		toolBar.sizeToFit()
		
		let doneButton = buttonItem
		let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
		
		toolBar.setItems([ spaceButton, doneButton], animated: false)
		toolBar.isUserInteractionEnabled = true
		
		return toolBar
	}
	
}
