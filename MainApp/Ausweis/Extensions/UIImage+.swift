//
//  UIImage+.swift
//  Ausweis
//
//  Created by Oleksandr Danylenko on 6/26/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

public extension UIImage {
    func save(named name:String) -> String? {
        guard let data = UIImageJPEGRepresentation(self, 1) ?? UIImagePNGRepresentation(self) else {
            return nil
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return nil
        }
        do {
            let path = directory.appendingPathComponent("\(name).png")!
            try data.write(to: path)
            return path.path
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
	
	public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
		let rect = CGRect(origin: .zero, size: size)
		UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
		color.setFill()
		UIRectFill(rect)
		let image = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		guard let cgImage = image?.cgImage else { return nil }
		self.init(cgImage: cgImage)
	}

}
