//
//  UILabel+.swift
//  Ausweis
//
//  Created by admin1 on 7/4/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit


extension UILabel
{
	func addImage(imageName: String, afterLabel bolAfterLabel: Bool = false)
	{
		let attachment: NSTextAttachment = NSTextAttachment()
		attachment.image = UIImage(named: imageName)
		let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
		
		if (bolAfterLabel)
		{
			let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string: self.text!)
			strLabelText.append(attachmentString)
			
			self.attributedText = strLabelText
		}
		else
		{
			let strLabelText: NSAttributedString = NSAttributedString(string: self.text!)
			let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
			mutableAttachmentString.append(strLabelText)
			
			self.attributedText = mutableAttachmentString
		}
	}
	
	func removeImage()
	{
		let text = self.text
		self.attributedText = nil
		self.text = text
	}
}
