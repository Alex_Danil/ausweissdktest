//
//  UIViewController+.swift
//  Ausweis
//
//  Created by admin1 on 7/25/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit

extension UIViewController {
	
	func add(_ child: UIViewController) {
		addChildViewController(child)
		child.view.frame = view.frame
		view.addSubview(child.view)
		child.didMove(toParentViewController: self)
	}
	
	func remove() {
		guard parent != nil else {
			return
		}
		willMove(toParentViewController: nil)
		removeFromParentViewController()
		view.removeFromSuperview()
	}
}
