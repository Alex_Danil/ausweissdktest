//
//  UIView+.swift
//  Ausweis
//
//  Created by admin1 on 6/20/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import UIKit


public extension UIView {
	@IBInspectable public var cornerRadius: CGFloat {
		get { return self.layer.cornerRadius }
		set { self.layer.cornerRadius = newValue }
	}
	
	@IBInspectable public var borderWidth: CGFloat {
		get { return self.layer.borderWidth }
		set { self.layer.borderWidth = newValue }
	}
	
	@IBInspectable public var borderColor: UIColor {
		get { return UIColor(cgColor: self.layer.borderColor!) }
		set { self.layer.borderColor = newValue.cgColor }
	}
}
