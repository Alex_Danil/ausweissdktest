//
//  Observable+.swift
//  Ausweis.ioSDKTest
//
//  Created by admin1 on 6/22/18.
//  Copyright © 2018 TIW. All rights reserved.
//

import Foundation
import RxSwift


public protocol OptionalType {
	associatedtype Wrapped
	
	var optional: Wrapped? { get }
}

extension Optional: OptionalType {
	public var optional: Wrapped? { return self }
}

// Unfortunately the extra type annotations are required, otherwise the compiler gives an incomprehensible error.
extension Observable where Element: OptionalType {
	func ignoreNil() -> Observable<Element.Wrapped> {
		return flatMap { value in
			value.optional.map { Observable<Element.Wrapped>.just($0) } ?? Observable<Element.Wrapped>.empty()
		}
	}
}

//extension Observable where Element == String {
//	func validate(_ regexp: String) -> Observable<Element> {
//		return flatMap { value in
//			value.optional.map { Observable<Element>.just($0) } ?? Observable<Element>.empty()
//		}
//	}
//}
